from django import forms
from captcha.fields import ReCaptchaField
from captcha.widgets import ReCaptchaV2Checkbox


class ContactForm(forms.Form):
    contact_name = forms.CharField(required=True, label="Twoje imię")
    contact_email = forms.EmailField(required=True, label="Twój Email")
    content = forms.CharField(required=True, widget=forms.Textarea, label="Wiadomość")
    captcha = ReCaptchaField(
        widget=ReCaptchaV2Checkbox(
            attrs={"data-callback": "enableBtn"}, api_params={"hl": "pl"}
        ),
        label="Autoryzacja",
    )

from contact.forms import ContactForm
from django.conf import settings
from django.core.mail import EmailMessage
from django.shortcuts import redirect, render
from django.template.loader import get_template
from django.urls import reverse
from django.views.generic.edit import FormView


class ContactView(FormView):
    form_class = ContactForm
    template_name = "contact/contact.html"
    success_url = "dziekujemy/"

    def form_valid(self, form):
        data = form.cleaned_data
        context = {
            "contact_name": data.get("contact_name", ""),
            "contact_email": data.get("contact_email", ""),
            "form_content": data.get("content", ""),
        }

        template = get_template("contact/contact_template.txt")
        content = template.render(context)

        email = EmailMessage(
            "Nowa wiadomość z zakładki Kontakt",
            content,
            settings.DEFAULT_FROM_EMAIL,
            [settings.DEFAULT_FROM_EMAIL, "sciezkizdrowia@gmail.com"],
            reply_to=[data.get("contact_email", "")],
        )
        email.send()
        return render(
            self.request,
            "thank_you.html",
            {"url": reverse("opinions:list"), "header": "Dziękujemy za wiadomość."},
        )


def contact(request):
    form_class = ContactForm

    if request.method == "POST":
        form = form_class(data=request.POST)

        if form.is_valid():
            contact_name = request.POST.get("contact_name", "")
            contact_email = request.POST.get("contact_email", "")
            form_content = request.POST.get("content", "")

            template = get_template("contact/contact_template.txt")
            context = {
                "contact_name": contact_name,
                "contact_email": contact_email,
                "form_content": form_content,
            }
            content = template.render(context)

            email = EmailMessage(
                "Nowa wiadomość z zakładki Kontakt",
                content,
                settings.DEFAULT_FROM_EMAIL,
                [settings.DEFAULT_FROM_EMAIL, "sciezkizdrowia@gmail.com"],
                reply_to=[contact_email],
            )
            email.send()
            return redirect("contact")

    return render(request, "contact/contact.html", {"form": form_class})

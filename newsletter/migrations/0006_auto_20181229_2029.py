# Generated by Django 2.0.3 on 2018-12-29 19:29

import ckeditor_uploader.fields
from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('newsletter', '0005_auto_20181009_1213'),
    ]

    operations = [
        migrations.AlterField(
            model_name='message',
            name='content',
            field=ckeditor_uploader.fields.RichTextUploadingField(),
        ),
    ]

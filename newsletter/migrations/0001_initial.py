# Generated by Django 2.0.3 on 2018-07-11 17:05

import ckeditor.fields
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Message',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=30, verbose_name='nazwa')),
                ('title', models.CharField(max_length=30, verbose_name='tytuł')),
                ('content', ckeditor.fields.RichTextField()),
            ],
        ),
        migrations.CreateModel(
            name='NewsLetter',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('modified', models.DateTimeField(auto_now=True)),
                ('newsletter_name', models.CharField(max_length=50, verbose_name='Nazwa newslettera')),
                ('message', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='newsletter.Message')),
            ],
            options={
                'abstract': False,
            },
        ),
    ]

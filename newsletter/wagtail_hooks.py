from django.utils.translation import ugettext_lazy as _
from wagtail.contrib.modeladmin.options import (
    ModelAdmin, ModelAdminGroup, modeladmin_register)

from newsletter.models import NewsLetterCampaign, NewsletterMessage


class NewsLetterModelAdmin(ModelAdmin):
    model = NewsLetterCampaign
    menu_label = _("Wysłane newslettery")
    menu_icon = 'success'
    list_display = ('newsletter_name', )
    search_fields = ('name', 'category')


class MessageModelAdmin(ModelAdmin):
    model = NewsletterMessage
    menu_label = _("Wiadomości newsletterów")
    menu_icon = 'form'
    list_display = ('name', )


class NewsLetterAdminGroup(ModelAdminGroup):
    menu_label = _("Newsletter")
    menu_icon = 'form'
    menu_order = 200
    items = (NewsLetterModelAdmin, MessageModelAdmin)


modeladmin_register(NewsLetterAdminGroup)

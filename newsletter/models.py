from django.db import models
from django.core.mail import send_mail
from decouple import config
from django.template.loader import render_to_string
from ckeditor_uploader.fields import RichTextUploadingField

from core.models import TimestampedModel
from core.tokens import deactivate_newsletter_token
from django.utils.translation import gettext_lazy as _
from core.field_validators import check_if_less_than_200_words
from django.utils.encoding import force_bytes
from django.utils.http import urlsafe_base64_encode


class NewsLetterCampaign(TimestampedModel):
    newsletter_name = models.CharField(_('Nazwa kampanii'), max_length=255, blank=False, null=False)
    message = models.ForeignKey(
        'newsletter.NewsletterMessage',
        on_delete=models.PROTECT,
    )

    @staticmethod
    def send_email(data, from_email=None):
        context = {'message': data.get('message_to_send').content}
        for user in data.get('users'):
            context['uid'] = urlsafe_base64_encode(force_bytes(user.pk)).decode()
            context['token'] = deactivate_newsletter_token.make_token(user)
            message = render_to_string('newsletter/newsletter_email.html', context=context)
            send_mail(data.get('message_to_send').title, message, from_email, [user.email], html_message=message)


class NewsletterMessage(models.Model):
    title = models.CharField(_('Tytuł wiadomości'), max_length=100, blank=False, null=False)
    content = RichTextUploadingField(validators=[check_if_less_than_200_words])

    class Meta:
        verbose_name = _("Wiadomość")
        verbose_name_plural = _("Wiadomości")

    def __str__(self):
        return self.title

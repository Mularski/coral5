from django.contrib import admin
from coral5.admin import site
from .models import NewsletterMessage


class NewsletterMessageAdmin(admin.ModelAdmin):
    change_form_template = 'newsletter/admin/change_form.html'

site.register(NewsletterMessage, NewsletterMessageAdmin)
from django.contrib.sites.shortcuts import get_current_site
from django.http import HttpResponseRedirect, HttpResponse
from django.http import JsonResponse
from django.shortcuts import redirect

from django.utils.encoding import force_bytes, force_text
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode
from django.template.loader import render_to_string
from django.views.decorators.csrf import csrf_exempt

from core.tokens import deactivate_newsletter_token
from django.core.mail import send_mail
from decouple import config
from django.contrib.auth.models import User
from core.models import CustomUser
from newsletter.models import NewsletterMessage


@csrf_exempt
def send_newsletter_welcome(request):
    if request.method == "POST":
        if request.user.id:
            auth_user = request.user
            auth_user.is_subscriber = True
            auth_user.save()

            message = render_to_string(
                "newsletter/welcome.html",
                {
                    "user": auth_user,
                    "domain": get_current_site(request).domain,
                    "uid": urlsafe_base64_encode(force_bytes(auth_user.pk)).decode(),
                    "token": deactivate_newsletter_token.make_token(auth_user),
                },
            )
            mail_subject = "Dziękujemy za zapisanie się do naszego Newslettera"

            send_mail(
                mail_subject,
                message,
                config("EMAIL_HOST_USER"),
                [auth_user.email],
                html_message=message,
            )

            # Wyślij zmienną 'status' żeby usunąć przycisk 'Zapisz do newslettera'
            data = {"status": "success"}
        else:
            data = {"status": "failure"}
        return JsonResponse(data)
    else:
        return redirect("/")


def newsletter_deactivate(request, uidb, token):
    try:
        uid = force_text(urlsafe_base64_decode(uidb))
        user = CustomUser.objects.get(pk=uid)
    except (TypeError, ValueError, OverflowError, User.DoesNotExist):
        user = None
    if user is not None and deactivate_newsletter_token.check_token(user, token):
        custom_user = CustomUser.objects.get(pk=user.pk)
        custom_user.is_subscriber = False
        custom_user.save()
    return HttpResponseRedirect("/")


def NewsletterMessageDetailsView(request, message_id):
    message = NewsletterMessage.objects.get(id=message_id)

    return HttpResponse(message.content)

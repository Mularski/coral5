from django.conf.urls import url
from django.urls import path

from . import views

app_name = "newsletter"

urlpatterns = [
    url(
        r"^ajax/send_newsletter_welcome/$",
        views.send_newsletter_welcome,
        name="send_newsletter_welcome",
    ),
    url(
        r"^newsletter_deactivate/(?P<uidb>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$",
        views.newsletter_deactivate,
        name="newsletter_deactivate",
    ),
    path(
        "ajax/message_details/<message_id>",
        views.NewsletterMessageDetailsView,
        name="message_details",
    ),
]

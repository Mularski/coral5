from django.contrib.sitemaps import Sitemap
from django.shortcuts import reverse
from kitchen.models import Recipe
from articles.models import Article
from supplement.models import Supplement
from news.models import News


class HomePageSitemap(Sitemap):
    changefreq = "never"
    priority = 1
    
    def items(self):
        return ['home:list']

    def location(self, item):
        return reverse(item)

class StaticViewSitemap(Sitemap):
    changefreq = "monthly"
    priority = 0.85
    
    def items(self):
        return ['about', 'contact', 'privacy-policy', 'terms-of-use', 'news:list', 'supplement:list']

    def location(self, item):
        return reverse(item)

class KitchenSitemap(Sitemap):
    changefreq = "monthly"
    priority = 0.7

    def items(self):
        return Recipe.objects.all()

class ArticlesSitemap(Sitemap):
    changefreq = "monthly"
    priority = 0.7
    def items(self):
        return Article.objects.all()

class ProductsSitemap(Sitemap):
    changefreq = "monthly"
    priority = 0.7

    def items(self):
        return Supplement.objects.all()

class NewsSitemap(Sitemap):
    changefreq = "monthly"
    priority = 0.7  

    def items(self):
        return News.objects.all()

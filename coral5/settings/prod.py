from .base import *

ALLOWED_HOSTS = ['.sciezkidozdrowia.pl']

SESSION_COOKIE_SECURE = True

CSRF_COOKIE_SECURE = True

SECURE_HSTS_SECONDS = 3600

SECURE_HSTS_INCLUDE_SUBDOMAINS = True

SECURE_HSTS_PRELOAD = True

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'coral5_prod',
        'USER': 'karol',
        'PASSWORD': 'klocki3782793',
        'HOST': 'localhost',
        'PORT': '',
    }

}

"""Sentry configuration"""
import sentry_sdk
from sentry_sdk.integrations.redis import RedisIntegration

sentry_sdk.init(
    dsn='https://f609c6e562884455889d05b7377c6184@o517026.ingest.sentry.io/5624228',
    integrations=[RedisIntegration()]
)

from .base import *

ALLOWED_HOSTS = ["*"]

DEBUG = True

# DEBUG TOOLBAR
# INSTALLED_APPS += ['debug_toolbar']
INSTALLED_APPS += ["django_extensions"]

# MIDDLEWARE += ['debug_toolbar.middleware.DebugToolbarMiddleware']

INTERNAL_IPS = ["127.0.0.1"]
# DEBUG TOOLBAR END

DATABASES = {
    "default": {
        "ENGINE": "django.db.backends.postgresql_psycopg2",
        "NAME": "coral5_dev",
        "USER": "karol",
        "PASSWORD": "klocki3782793",
        "HOST": "localhost",
        "PORT": "",
    }
}

EMAIL_BACKEND = "django.core.mail.backends.console.EmailBackend"

from django.contrib.sitemaps.views import sitemap
from django.conf import settings
from django.conf.urls import include, url
from django.conf.urls.static import static
from django.contrib.auth import views as auth_views
from django.urls import path, re_path
from django.views.generic import TemplateView
from profiles.views import (
    CustomFormDetail,
    FormEntryDetailView,
    FormListView,
    InvitedFormListView,
    download_view,
    download_view_from_folder,
    form_entry_update_view,
    form_sent,
    signup_view,
)
from wagtail.admin import urls as wagtailadmin_urls
from wagtail.core import urls as wagtail_urls
from wagtail.documents import urls as wagtaildocs_urls
from core.views import navbar

from contact.views import ContactView
from coral5 import sitemaps
from coral5.admin import site
from core.views import lading_page_detail_view


sitemaps = {
    'home': sitemaps.HomePageSitemap,
    'static': sitemaps.StaticViewSitemap,
    'kitchen': sitemaps.KitchenSitemap,
    'articles': sitemaps.ArticlesSitemap,
    'news': sitemaps.NewsSitemap,
}


urlpatterns = [
    path('sitemap.xml', sitemap, {'sitemaps': sitemaps},
     name='django.contrib.sitemaps.views.sitemap'),
    path(
        "robots.txt",
        TemplateView.as_view(template_name="robots.txt", content_type="text/plain"),
    ),
    path("cms/", include(wagtailadmin_urls)),
    path("documents/", include(wagtaildocs_urls)),
    path("pages/", include(wagtail_urls)),
    path("MyZ5C5Fc8r/", site.urls),
    path("o-nas/", TemplateView.as_view(template_name="about.html"), name="about"),
    path("kontakt/", ContactView.as_view(), name="contact"),
    path(
        "kontakt/dziekujemy/",
        TemplateView.as_view(template_name="thank_you.html"),
        name="thank_you",
    ),
    path(
        "polityka-prywatnosci/",
        TemplateView.as_view(template_name="privacy_policy.html"),
        name="privacy-policy",
    ),
    path(
        "zasady-korzystania/",
        TemplateView.as_view(template_name="terms_of_use.html"),
        name="terms-of-use",
    ),
    path(
        "dziekujemy/",
        TemplateView.as_view(template_name="thank_you_reservation.html"),
        name="thank_you_reservation",
    ),
    path(
        "zmien-haslo/", auth_views.PasswordChangeView.as_view(), name="password_change"
    ),
    path(
        "zmien-haslo/sukces/",
        auth_views.PasswordChangeDoneView.as_view(),
        name="password_change_done",
    ),
    path("reset-hasla/", auth_views.PasswordResetView.as_view(), name="password_reset"),
    path(
        "reset-hasla/sukces/",
        auth_views.PasswordResetDoneView.as_view(),
        name="password_reset_done",
    ),
    path(
        "reset/<uidb64>/<token>/",
        auth_views.PasswordResetConfirmView.as_view(),
        name="password_reset_confirm",
    ),
    path(
        "reset/done/",
        auth_views.PasswordResetCompleteView.as_view(),
        name="password_reset_complete",
    ),
    path(
        "kalkulator/",
        TemplateView.as_view(template_name="calculator.html"),
        name="calculator",
    ),
    path("zarejestruj/<int:coral_id>/", signup_view, name="signup"),
    path("zarejestruj/", signup_view, name="signup"),
    path("", include("django.contrib.auth.urls")),
    path(
        "sukces",
        TemplateView.as_view(template_name="registration/signup_success.html"),
        name="signup-success",
    ),
    path(
        "niepowodzenie",
        TemplateView.as_view(template_name="registration/bad_token.html"),
        name="bad-token",
    ),
    path("ankiety/", FormListView.as_view(), name="form-list"),
    path("ankiety/<pk>", FormEntryDetailView.as_view(), name="filled_form"),
    path("ankiety/edytuj/<pk>", form_entry_update_view, name="form-update"),
    path(
        "ankiety/podopieczni/<email>",
        InvitedFormListView.as_view(),
        name="invited-form-list",
    ),
    re_path(
        r"ankiety/(?P<slug>.*)/sent/$", form_sent, name="form_sent"
    ),  # nadpisany widok
    re_path(r"ankiety/(?P<slug>.*)/$", CustomFormDetail.as_view(), name="form_detail"),
    path("lp/<slug>/", lading_page_detail_view, name="landing-page"),
    path("kuchnia/", include("kitchen.urls", namespace="kitchen")),
    path("encyklopedia/", include("articles.urls", namespace="articles")),
    path("produkty/", include("supplement.urls", namespace="supplements")),
    path("aktualnosci/", include("news.urls", namespace="news")),
    path("spotkania/", include("news.event_urls", namespace="events")),
    path("profil/", include("profiles.urls", namespace="profile")),
    path("navbar/", navbar, name="navbar"),
    path("", include("opinions.urls", namespace="home")),
    path("", include("newsletter.urls", namespace="newsletter")),
    path("tinymce/", include("tinymce.urls")),
    path("ckeditor/", include("ckeditor_uploader.urls")),
    path(
        "download/forms/<path>/<file_name>",
        download_view_from_folder,
        name="download_from_folder",
    ),
    path("download/<path>", download_view, name="download"),
]

if settings.DEBUG:
    try:
        import debug_toolbar

        urlpatterns += [url(r"^__debug__/", include(debug_toolbar.urls))]
    except:
        pass
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

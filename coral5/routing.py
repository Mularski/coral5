from django.conf.urls import url
from channels.routing import ProtocolTypeRouter, URLRouter
from channels.auth import AuthMiddlewareStack
from channels.security.websocket import AllowedHostsOriginValidator, OriginValidator

from chat.consumers import ChatConsumer

application = ProtocolTypeRouter({
    # AllowedHostsOriginValidator sprawdza czy domena z requesta znajduje się w ALLOWED_HOSTS
    # AuthMiddlewareStack sprawdza czy user jest zalogowany
    'websocket': AllowedHostsOriginValidator(
        AuthMiddlewareStack(
            URLRouter(
                [
                    url(r'^profil/chat/(?P<email>[\w.@+-]+)/$', ChatConsumer)
                ]
            )
        )
    )
})

import datetime

import re

from django.core.mail import send_mail
from django.shortcuts import render, redirect
from django.views.generic import DetailView, ListView

from core.models import LandingPage
from news.models import Event2
from .models import Opinion, Coop


def opinion_list_view(request):
    context = {'landing_pages': LandingPage.objects.filter(is_published=True),
               'event_list': Event2.objects.filter(event_date__gte=datetime.date.today()).order_by('event_date')[:5],
               'opinion_list': Opinion.objects.all(),
               'coop': Coop.objects.first(),
               }
    return render(request, 'opinions/opinion_list.html', context=context)


class OpinionListView(ListView):

    def get_landing_pages(self):
        if self.request.user.is_staff:
            return LandingPage.objects.all()
        else:
            return LandingPage.objects.filter(is_published=True)

    def get_queryset(self):
        return Opinion.objects.all()

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['event_list'] = Event2.objects.filter(event_date__gte=datetime.date.today()).order_by('event_date')[:5]
        context['coop'] = Coop.objects.first()
        return context


class OpinionDetailView(DetailView):
    queryset = Opinion.objects.all()

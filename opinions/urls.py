from django.urls import path

from .views import OpinionDetailView, opinion_list_view

app_name = "opinions"

urlpatterns = [
    path(r'<slug>/', OpinionDetailView.as_view(), name="detail"),
    path(r'', opinion_list_view, name="list"),
]

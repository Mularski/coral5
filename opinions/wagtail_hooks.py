from django.utils.translation import ugettext_lazy as _
from wagtail.contrib.modeladmin.options import (
    ModelAdmin, ModelAdminGroup, modeladmin_register)

from opinions.models import Opinion, Coop


class OpinionModelAdmin(ModelAdmin):
    model = Opinion
    menu_label = _("Opinie")
    menu_icon = 'group'
    list_display = ('name', 'author', 'timestamp')
    search_fields = ('name', 'author')


class CoopModelAdmin(ModelAdmin):
    model = Coop
    menu_label = _("Nagłówek sprzedażowy")
    menu_icon = 'form'
    list_display = ('header', )


class OpinionAdminGroup(ModelAdminGroup):
    menu_label = _("Opinie")
    menu_icon = 'form'
    menu_order = 200
    items = (OpinionModelAdmin, CoopModelAdmin)


modeladmin_register(OpinionAdminGroup)

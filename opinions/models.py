from django.db import models
from django.urls import reverse
from ckeditor_uploader.fields import RichTextUploadingField
from articles.utils import unique_slug_generator
from django.db.models.signals import pre_save


class Coop(models.Model):
    header = models.CharField(max_length=100, help_text='Nagłówek', default='Jakiś nagłówek',
                              blank=False, null=False)
    description = RichTextUploadingField()

    class Meta:
        verbose_name = 'Nagłówek sprzedażowy'
        verbose_name_plural = 'Nagłówki sprzedażowe'


class Opinion(models.Model):
    name = models.CharField(max_length=120)
    slug = models.SlugField(max_length=255, blank=True, null=True)
    author = models.CharField(max_length=50)
    timestamp = models.DateTimeField(auto_now_add=True)
    description = RichTextUploadingField()

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse('home:detail', kwargs={"slug": self.slug})

    class Meta:
        ordering = ["-name", "-timestamp"]
        verbose_name = "opinia"
        verbose_name_plural = "opinie"


def rl_pre_save_receiver(sender, instance, *args, **kwargs):
    
    if not instance.slug:
        instance.slug = unique_slug_generator(instance)


pre_save.connect(rl_pre_save_receiver, sender=Opinion)

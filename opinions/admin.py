from coral5.admin import site
from .models import Opinion, Coop

site.register(Opinion)
site.register(Coop)

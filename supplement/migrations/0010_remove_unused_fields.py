import django.contrib.postgres.fields
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('supplement', '0009_auto_20201123_2302'),
    ]

    operations = [
         migrations.RemoveField(
            model_name='supplement',
            name='opinions',
        ),
        migrations.RemoveField(
            model_name='supplement',
            name='related_articles',
        ),
        migrations.RemoveField(
            model_name='supplement',
            name='related_products',
        ),
    ]

from captcha.fields import ReCaptchaField
from captcha.widgets import ReCaptchaV2Checkbox
from django.views.generic import DetailView, ListView
from supplement.models import Supplement, SupplementCategory
from django import forms

from django.shortcuts import render
from django.core.mail import EmailMessage
from django.template.loader import get_template
from django.conf import settings

from contact.forms import ContactForm
from django.urls import reverse
from django.views.generic.edit import FormMixin


class SupplementListView(ListView):
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["categories"] = SupplementCategory.objects.all()
        context["dict_sort"] = "name"
        context["show"] = "portion"
        return context

    def get_queryset(self):
        if self.request.GET.get("category"):
            cat = self.request.GET.get("category")
            cat_obj = SupplementCategory.objects.get(slug=cat)
            return Supplement.objects.filter(category=cat_obj)
        else:
            return Supplement.objects.all()


class ContactForm(forms.Form):
    contact_name = forms.CharField(required=True, label="Twoje imię")
    contact_email = forms.EmailField(required=True, label="Twój Email")
    content = forms.CharField(required=True, widget=forms.Textarea, label="Wiadomość")
    captcha = ReCaptchaField(
        widget=ReCaptchaV2Checkbox(
            attrs={"data-callback": "enableBtn"}, api_params={"hl": "pl"}
        ),
        label="Autoryzacja",
    )
    product_slug = forms.CharField(widget=forms.HiddenInput())


class SupplementDetailView(DetailView, FormMixin):
    model = Supplement
    form_class = ContactForm

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["form"] = ContactForm(initial={"product_slug": context["object"].slug})
        return context

    # def get_success_url(self):
    #     return render('opinions:list', kwargs={'pk': self.object.id})

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        form = self.get_form()
        if form.is_valid():
            return self.form_valid(form)
        else:
            return self.form_invalid(form)

    def form_valid(self, form):
        data = form.cleaned_data
        product = Supplement.objects.get(slug=data.get("product_slug", ""))
        context = {
            "contact_name": data.get("contact_name", ""),
            "contact_email": data.get("contact_email", ""),
            "form_content": data.get("content", ""),
        }

        template = get_template("contact/contact_template.txt")
        content = template.render(context)

        email = EmailMessage(
            "Nowa wiadomość - {}".format(product.name),
            content,
            settings.DEFAULT_FROM_EMAIL,
            [settings.DEFAULT_FROM_EMAIL, "sciezkizdrowia@gmail.com"],
            reply_to=[data.get("contact_email", "")],
        )
        email.send()
        return render(
            self.request, "thank_you.html", {"url": reverse("supplement:list")}
        )

from django.apps import apps
from django.urls import reverse
from django.utils.translation import ugettext_lazy as _
from ckeditor_uploader.fields import RichTextUploadingField
from articles.utils import unique_slug_generator
from django.db import models
from django.db.models import Case, When
from django.db.models.signals import pre_save

from opinions.models import Opinion
from django.contrib.postgres.fields.array import ArrayField


class SupplementCategory(models.Model):
    name = models.CharField(_("Nazwa kategorii"), max_length=100, blank=False, null=False, default='kategoria')
    slug = models.SlugField(max_length=255, blank=True, null=True)
    
    def __str__(self):
        return self.name
    
    class Meta:
        ordering = ['-name']
        verbose_name = _("Kategoria suplementów")
        verbose_name_plural = _("Kategorie suplementów")
    

class Supplement(models.Model):
    name = models.CharField(_("Nazwa"), blank=False, null=False, help_text=_("product name"), max_length=50,
                            default='Nazwa Produktu')
    category = models.ForeignKey(SupplementCategory, on_delete=models.SET_NULL, verbose_name=_("Kategoria"), null=True)
    price = models.IntegerField(_("Cena"), blank=True, null=True)
    portion = models.CharField(_("Porcja"), max_length=100, blank=True, null=True)
    img = models.ImageField(_("Zdjęcie"), default='avatar/No-image-available.jpg', upload_to='products/', blank=True,
                            null=True)
    description = RichTextUploadingField(verbose_name=_("Opis"))
    contains = models.TextField(_("Zawiera"), blank=True, null=True)
    slug = models.SlugField(max_length=255, blank=True, null=True)
    opinions_order = ArrayField(models.IntegerField(), default=list, verbose_name=_("Powiązane opinie"))
    products_order = ArrayField(models.IntegerField(), default=list, verbose_name=_("Powiązane produkty"))
    articles_order = ArrayField(models.IntegerField(), default=list, verbose_name=_("Powiązane artykuły"))

    def __str__(self):
        return self.name

    @property
    def opinions(self):
        ids = self.opinions_order
        preserved = Case(*[When(pk=pk, then=pos) for pos, pk in enumerate(ids)])
        return Opinion.objects.filter(id__in=ids).order_by(preserved)

    @property
    def related_products(self):
        ids = self.products_order
        preserved = Case(*[When(pk=pk, then=pos) for pos, pk in enumerate(ids)])
        return self.__class__.objects.filter(id__in=ids).order_by(preserved)

    @property
    def related_articles(self):
        ids = self.articles_order
        preserved = Case(*[When(pk=pk, then=pos) for pos, pk in enumerate(ids)])
        return apps.get_model(app_label='articles', model_name='Article').objects.filter(id__in=ids).order_by(preserved)

    @property
    def image_url(self):
        if self.image and hasattr(self.image, 'url'):
            return self.image.url
        return '#'

    def get_absolute_url(self):
        return reverse('supplements:detail', kwargs={"slug": self.slug})

    class Meta:
        ordering = ['-name']
        verbose_name = _("Suplement")
        verbose_name_plural = _("Suplementy")


def rl_pre_save_receiver(sender, instance, *args, **kwargs):
    if not instance.slug:
        instance.slug = unique_slug_generator(instance)


pre_save.connect(rl_pre_save_receiver, sender=Supplement)
pre_save.connect(rl_pre_save_receiver, sender=SupplementCategory)

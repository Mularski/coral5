from django.utils.translation import ugettext_lazy as _
from wagtail.contrib.modeladmin.options import (
    ModelAdmin, ModelAdminGroup, modeladmin_register)

from supplement.models import Supplement, SupplementCategory


class SupplementModelAdmin(ModelAdmin):
    model = Supplement
    menu_label = _("Suplementy")
    menu_icon = 'snippet'
    list_display = ('name', 'category')
    search_fields = 'name'
    list_filter = ('category', )


class SupplementCategoryModelAdmin(ModelAdmin):
    model = SupplementCategory
    menu_label = _("Kategorie suplementów")
    menu_icon = 'snippet'
    list_display = ('name', )


class SupplementAdminGroup(ModelAdminGroup):
    menu_label = _("Suplementy")
    menu_icon = 'form'
    menu_order = 200
    items = (SupplementModelAdmin, SupplementCategoryModelAdmin)


modeladmin_register(SupplementAdminGroup)

from coral5.admin import site
from .forms import SupplementForm
from .models import Supplement, SupplementCategory
from django.contrib import admin


class SupplementCategoryAdmin(admin.ModelAdmin):
    exclude = ('slug',)


class SupplementAdmin(admin.ModelAdmin):
    form = SupplementForm


site.register(SupplementCategory, SupplementCategoryAdmin)
site.register(Supplement, SupplementAdmin)

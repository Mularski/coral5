from django.conf.urls import url
from django.urls import path
from django.views.decorators.http import require_POST
from supplement.views import SupplementDetailView, SupplementListView

app_name = "supplement"

urlpatterns = [
    path('<slug>/', SupplementDetailView.as_view(), name='detail'),
    path('', SupplementListView.as_view(), name='list')
]

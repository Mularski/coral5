from coral5.admin import site
from django.contrib import admin
from kitchen.models import Recipe, RecipeCategory

from .forms import ArticleForm
from .models import Article, ArticleCategory


class ModelAdminWithoutSlug(admin.ModelAdmin):
    exclude = ("slug",)


class ArticleAdmin(admin.ModelAdmin):
    form = ArticleForm
    list_display = ('name', 'category')
    list_filter = ('category',)


site.register(ArticleCategory, ModelAdminWithoutSlug)

site.register(Article, ArticleAdmin)

from wagtail.contrib.modeladmin.options import (
    ModelAdmin, ModelAdminGroup, modeladmin_register)
from django.utils.translation import ugettext_lazy as _

from articles.models import ArticleCategory, Article


class ArticleModelAdmin(ModelAdmin):
    model = Article
    menu_label = _("Artykuły")
    menu_icon = 'doc-empty-inverse'
    list_display = ('name', 'category', 'timestamp')
    list_filter = ('category',)
    search_fields = ('name', 'category', 'timestamp')


class ArticleCategoryModelAdmin(ModelAdmin):
    model = ArticleCategory
    menu_label = _("Kategorie artykułów")
    menu_icon = 'doc-empty'
    list_display = ('name', )


class ArticleAdminGroup(ModelAdminGroup):
    menu_label = _("Artykuły")
    menu_icon = 'doc-empty-inverse'
    menu_order = 200
    items = (ArticleModelAdmin, ArticleCategoryModelAdmin)


modeladmin_register(ArticleAdminGroup)

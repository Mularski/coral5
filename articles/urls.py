from django.urls import path
from .views import article_detail, article_list

app_name = "articles"

urlpatterns = [
    path('<category>/<slug>/', article_detail, name='detail'),
    path('<category>/', article_list, name='category_list'),
]

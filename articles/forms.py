from django import forms
from django.utils.translation import ugettext_lazy as _


from .models import Article
from supplement.models import Supplement
from opinions.models import Opinion
from core.fields import SharedListFormField
from core.widgets import SharedListWidget


class ArticleForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['articles_order'] = SharedListFormField(model=Article,
                                                            query=self.instance.articles_order,
                                                            label=_('Powiązane artykuły'),
                                                            required=False)
        self.fields['products_order'] = SharedListFormField(model=Supplement,
                                                            query=self.instance.products_order,
                                                            label=_('Powiązane produkty'),
                                                            required=False)
        self.fields['opinions_order'] = SharedListFormField(model=Opinion,
                                                            query=self.instance.opinions_order,
                                                            label=_('Powiązane opinie'),
                                                            required=False)
    
    class Meta:
        model = Article
        exclude = ['slug', 'timestamp']

from articles.utils import unique_slug_generator
from ckeditor_uploader.fields import RichTextUploadingField
from django.contrib.postgres.fields import ArrayField
from django.db import models
from django.db.models.signals import pre_save
from django.db.models import Case, When
from django.urls import reverse
from django.utils.translation import ugettext_lazy as _
from opinions.models import Opinion
from supplement.models import Supplement


class ArticleCategory(models.Model):
    name = models.CharField(
        _("Tytuł"),
        max_length=255,
        default="Bez kategorii",
    )
    img = models.ImageField(
        upload_to="articles/",
        default="articles/placeholder.jpg",
        blank=True,
        null=True,
        verbose_name=_("Zdjęcie"),
    )
    description = models.TextField(_("Opis"), default="")
    slug = models.SlugField(max_length=255, blank=True, null=True)

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse("articles:category_list", kwargs={"category": self.slug})

    class Meta:
        ordering = ["-name"]
        verbose_name = "Kategoria artykułu"
        verbose_name_plural = "Kategorie artykułów"


class Article(models.Model):
    name = models.CharField(_("Tytuł"), max_length=120)
    slug = models.SlugField(max_length=255, blank=True, null=True)
    category = models.ForeignKey(
        ArticleCategory, verbose_name=_("Kategoria"), on_delete=models.CASCADE
    )
    timestamp = models.DateTimeField(auto_now_add=True)
    description = RichTextUploadingField(_("Opis"))
    img = models.ImageField(_("Zdjęcie"), upload_to='articles/', default='articles/placeholder.jpg', blank=True,
                            null=True)
    public = models.BooleanField(_("Opublikowany"), default=True, help_text="Zaznacz, jeżeli niezalogowani też mogą "
                                                                            "obejrzeć")
    opinions_order = ArrayField(models.IntegerField(), default=list, verbose_name=_("Powiązane opinie"))
    products_order = ArrayField(models.IntegerField(), default=list, verbose_name=_("Powiązane produkty"))
    articles_order = ArrayField(models.IntegerField(), default=list, verbose_name=_("Powiązane artykuły"))

    @property
    def image_url(self):
        if self.image and hasattr(self.image, 'url'):
            return self.image.url
        return '#'

    def __str__(self):
        return self.name

    @property
    def opinions(self):
        ids = self.opinions_order
        preserved = Case(*[When(pk=pk, then=pos) for pos, pk in enumerate(ids)])
        return Opinion.objects.filter(id__in=ids).order_by(preserved)

    @property
    def related_products(self):
        ids = self.products_order
        preserved = Case(*[When(pk=pk, then=pos) for pos, pk in enumerate(ids)])
        return Supplement.objects.filter(id__in=ids).order_by(preserved)

    @property
    def related_articles(self):
        ids = self.articles_order
        preserved = Case(*[When(pk=pk, then=pos) for pos, pk in enumerate(ids)])
        return self.__class__.objects.filter(id__in=ids).order_by(preserved)

    def get_absolute_url(self):

        return reverse(
            "articles:detail",
            kwargs={"slug": self.slug, "category": self.category.slug},
        )

    class Meta:
        ordering = ["-timestamp", "-name"]
        verbose_name = "Artykuł"
        verbose_name_plural = "Artykuły"


def rl_pre_save_receiver(sender, instance, *args, **kwargs):
    if not instance.slug:
        instance.slug = unique_slug_generator(instance)


pre_save.connect(rl_pre_save_receiver, sender=Article)
pre_save.connect(rl_pre_save_receiver, sender=ArticleCategory)

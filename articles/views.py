from django.shortcuts import render

from core.models import CustomUser
from .models import Article, ArticleCategory


def article_list(request, category="Bez kategorii"):

    """Lista artykułów danej kategorii"""
    # Sprawdza czy kategoria 'top-secret', czy jest sie zalogowanym i czy jest sie adminem/płatnym userem
    if category == 'top-secret':
        if hasattr(request.user, 'is_paid'):
            if request.user.is_superuser or request.user.is_staff or request.user.is_paid:
                cat_id = ArticleCategory.objects.get(slug=category)
                object_list = Article.objects.filter(category=cat_id)
                return render(request, 'tile_list.html', {'object_list': object_list, 'dict_sort': "id", 'show': "description"})

        return render(request, 'articles/please_signup.html', {})

    cat_id = ArticleCategory.objects.get(slug=category)
    object_list = Article.objects.filter(category=cat_id)
    return render(request, 'tile_list.html', {'object_list': object_list, 'dict_sort': "id", 'show': "description"})


def article_detail(request, category, slug):
    if category == 'top-secret' and request.user.is_authenticated:
        try:
            if request.user.is_paid or request.user.is_superuser or request.user.is_staff:
                return render(request, 'articles/article_detail.html',
                              {'article': Article.objects.get(slug=slug)})
        except:
            return render(request, 'articles/please_signup.html')
    if category == 'top-secret' and not request.user.is_authenticated:
        return render(request, 'articles/please_signup.html')
    else:
        return render(request, 'articles/article_detail.html',
                      {'article': Article.objects.get(slug=slug)})

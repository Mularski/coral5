import django.contrib.postgres.fields
from django.db import migrations, models

def move_opinions(apps, schema_editor):
    Articles = apps.get_model('articles', 'Article')
    for article in Articles.objects.exclude(opinions__isnull=True).prefetch_related('opinions'):
        ids = article.opinions.all().values_list('id', flat=True)
        ids_list = list(ids)
        article.opinions_order = ids_list
        article.save()

class Migration(migrations.Migration):

    dependencies = [
        ('articles', '0013_auto_20201031_1445')
    ]

    operations =[
        migrations.AddField(
            model_name='article',
            name='opinions_order',
            field=django.contrib.postgres.fields.ArrayField(base_field=models.IntegerField(), default=list, blank=True, null=True, size=None, verbose_name='Powiązane opinie'),
        ),
        migrations.RunPython(move_opinions)
    ]
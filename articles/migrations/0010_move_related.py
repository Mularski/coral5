from django.db import migrations, models

def move_related_products(apps, schema_editor):
    Articles = apps.get_model('articles', 'Article')
    for article in Articles.objects.exclude(related_products__isnull=True).prefetch_related('related_products'):
        ids = article.related_products.all().values_list('id', flat=True)
        ids_list = list(ids)
        article.products_order = ids_list
        article.save()

class Migration(migrations.Migration):

    dependencies = [
        ('articles', '0009_auto_20201031_1117')
    ]

    operations =[
        migrations.RunPython(move_related_products)
    ]
# Generated by Django 2.0.3 on 2019-11-11 20:36

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('articles', '0004_auto_20191111_2134'),
    ]

    operations = [
        migrations.AlterField(
            model_name='article',
            name='opinions',
            field=models.ManyToManyField(to='opinions.Opinion', verbose_name='Opinie'),
        ),
        migrations.AlterField(
            model_name='article',
            name='related_products',
            field=models.ManyToManyField(to='supplement.Supplement', verbose_name='Powiązane produkty'),
        ),
    ]

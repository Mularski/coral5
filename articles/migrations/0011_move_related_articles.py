import django.contrib.postgres.fields
from django.db import migrations, models

def move_related_articles(apps, schema_editor):
    Articles = apps.get_model('articles', 'Article')
    for article in Articles.objects.exclude(related_articles__isnull=True).prefetch_related('related_articles'):
        ids = article.related_articles.all().values_list('id', flat=True)
        ids_list = list(ids)
        article.articles_order = ids_list
        article.save()

class Migration(migrations.Migration):

    dependencies = [
        ('articles', '0010_move_related')
    ]

    operations =[
        migrations.RunPython(move_related_articles)
    ]
from django.core.exceptions import ValidationError

CATEGORIES = ['Woda', 'Dieta', 'Rodzicielstwo', 'Cztery Kroki']


def validate_category(value):
    cat = value.capitalize()
    if value not in CATEGORIES and cat not in CATEGORIES:
        raise ValidationError("{value} not a valid category".format(value=value))

from articles.utils import unique_slug_generator
from ckeditor_uploader.fields import RichTextUploadingField
from django.conf import settings
from django.contrib.auth.models import AbstractUser, BaseUserManager
from django.db import models
from django.db.models.signals import pre_save
from django.urls import reverse
from django.utils.translation import ugettext_lazy as _
from forms_builder.forms.models import FormEntry


class CustomUserManager(BaseUserManager):
    use_in_migrations = True

    def _create_user(self, email, password, **extra_fields):
        if not email:
            raise ValueError("The given email must be set")
        email = self.normalize_email(email)
        user = self.model(email=email, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, email, password=None, **extra_fields):
        extra_fields.setdefault("is_staff", False)
        extra_fields.setdefault("is_superuser", False)
        return self._create_user(email, password, **extra_fields)

    def create_superuser(self, email, password, **extra_fields):
        extra_fields.setdefault("is_staff", True)
        extra_fields.setdefault("is_superuser", True)

        if extra_fields.get("is_staff") is not True:
            raise ValueError("superuser must be is_staff=True")
        if extra_fields.get("is_superuser") is not True:
            raise ValueError("superuser must be is_superuser=True")

        return self._create_user(email, password, **extra_fields)


class CustomUser(AbstractUser):
    username = None
    email = models.EmailField(_("Adres email"), max_length=255, unique=True)
    coral_id = models.IntegerField(_("Coral ID"), blank=True, null=True)
    is_paid = models.BooleanField(
        default=False,
        verbose_name=_("Opłacony dostęp"),
        help_text="Opłacony dostęp do sekcji Top Secret",
    )
    avatar = models.ImageField(
        default="avatar/No-image-available.jpg",
        upload_to="avatar/",
        blank=True,
        null=True,
    )
    invited_by = models.ForeignKey(
        "self",
        blank=True,
        null=True,
        verbose_name=_("Zaproszony przez"),
        on_delete=models.SET_NULL,
        related_name="inviting",
    )
    first_name = models.CharField(_("Imię"), max_length=255, blank=False, null=False)
    last_name = models.CharField(_("Nazwisko"), max_length=255, blank=False, null=False)
    is_subscriber = models.BooleanField(
        _("Subskrybent"),
        default=False,
        blank=True,
        help_text="Zasubskrybowano do newsletter'a",
    )
    can_add_event = models.BooleanField(
        _("Może dodawać eventy"), default=False, blank=True
    )
    tel_no = models.CharField(_("Numer telefonu"), blank=True, null=True, max_length=15)
    unsubscribe_hash = models.CharField(
        _("Hash do wypisania z newslettera"), max_length=255, default="No hash"
    )

    objects = CustomUserManager()

    USERNAME_FIELD = "email"
    REQUIRED_FIELDS = []

    class Meta:
        verbose_name = _("Użytkownik")
        verbose_name_plural = _("Użytkownicy")

    @staticmethod
    def get_absolute_url():
        return reverse("profile:user-profile")

    def has_module_perms(self, app_label):
        return True

    def has_perm(self, perm, obj=None):
        return True


class TimestampedModel(models.Model):
    created = models.DateTimeField(
        _("Utworzono"), auto_now_add=True, blank=True, null=True
    )
    modified = models.DateTimeField(
        _("Zmodyfikowano"), auto_now=True, blank=True, null=True
    )

    class Meta:
        abstract = True


class LocationModel(models.Model):
    country = models.CharField(_("Kraj"), max_length=255, blank=True, null=False)
    city = models.CharField(_("Miasto"), max_length=255, blank=True, null=False)
    street = models.CharField(_("Ulica"), max_length=255, blank=True, null=False)
    zip_code = models.CharField(
        _("Kod pocztowy"), max_length=255, blank=True, null=False
    )

    class Meta:
        abstract = True


class LandingPage(TimestampedModel):
    promo_image = models.ImageField(
        _("Zdjęcie promocyjne"),
        default="promo_images/No-image-available.jpg",
        upload_to="promo_images/",
        blank=False,
        null=False,
    )
    promo_header = models.CharField(
        _("Nagłówek promocyjny"),
        max_length=255,
        default="Wpisz tekst promocyjny",
        blank=True,
        null=True,
    )
    show_header = models.BooleanField(_("Pokaż naŋłówek"), default=False)
    slug = models.SlugField(max_length=255, blank=True, null=True)
    promo_text = models.CharField(
        _("Tekst promocyjny"),
        max_length=255,
        default="Wpisz tekst promocyjny",
        blank=True,
        null=True,
    )
    is_published = models.BooleanField(_("Opublikuj"), default=False)
    ask_for_info = models.BooleanField(_("Poproś o dane kontaktowe"), default=False)
    description = RichTextUploadingField(verbose_name=_("Treść Landing Page'a"))

    def __str__(self):
        return str(self.created)

    class Meta:
        verbose_name = _("Landing Page")
        verbose_name_plural = _("Landing Page")

    def get_absolute_url(self):
        return reverse("landing-page", kwargs={"slug": self.slug})


def pre_save_add_slug_receiver(sender, instance, *args, **kwargs):
    if not instance.slug:
        instance.slug = unique_slug_generator(instance)


pre_save.connect(pre_save_add_slug_receiver, sender=LandingPage)


class FormEntryUser(models.Model):
    """Tabela relacji między uzupełnionymi ankietami a użytkownikami"""

    form_entry = models.ForeignKey(
        FormEntry, verbose_name=_("Uzupełniona ankieta"), on_delete=models.CASCADE
    )
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        verbose_name=_("Użytkownik"),
        on_delete=models.CASCADE,
        related_name="form_entries",
    )
    is_public = models.BooleanField(_("Widoczna dla opiekuna"), default=True)

    def __str__(self):
        return str(self.user) + " " + str(self.form_entry.entry_time)

    class Meta:
        verbose_name = _("Uzupełniona ankieta")
        verbose_name_plural = _("Uzupełnione ankiety")

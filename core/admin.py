from coral5.admin import site
from django.contrib import admin
from django.contrib.admin import ModelAdmin
from django.contrib.auth.admin import UserAdmin
from django.db.models import Q
from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.utils.crypto import get_random_string
from django.utils.translation import ugettext_lazy as _
from forms_builder.forms.models import Field
from newsletter.models import NewsLetterCampaign, NewsletterMessage

from .models import CustomUser, FormEntryUser, LandingPage


class CustomUserAdmin(UserAdmin):
    fieldsets = (
        (None, {"fields": ("email", "password")}),
        (
            _("Personal info"),
            {
                "fields": (
                    "first_name",
                    "last_name",
                    "coral_id",
                    "invited_by",
                    "avatar",
                    "is_paid",
                    "is_subscriber",
                )
            },
        ),
        (_("Important dates"), {"fields": ("last_login", "date_joined")}),
    )
    add_fieldsets = (
        (
            None,
            {
                "classes": ("wide",),
                "fields": ("email", "password1", "password2"),
            },
        ),
    )
    list_display = (
        "email",
        "coral_id",
        "first_name",
        "last_name",
        "is_staff",
        "is_subscriber",
    )
    list_filter = ("is_staff", "is_superuser", "is_active", "is_subscriber")
    search_fields = ("coral_id", "first_name", "last_name", "email")
    ordering = ("email",)

    actions = ["send_newsletter"]

    def send_newsletter(self, request, queryset):

        if "send" in request.POST:
            for user in queryset:
                CustomUser.objects.filter(email=user).filter(
                    ~Q(is_subscriber=1)
                ).filter(unsubscribe_hash__isnull=True).update(
                    unsubscribe_hash=get_random_string(length=32)
                )
            try:
                message_obj = NewsletterMessage.objects.create(
                    title=request.POST.get("newsletter_name"),
                    content=request.POST.get("message_obj"),
                )
            except:
                message_obj = False
            if not message_obj:
                self.message_user(request, "Message not found", "warning")
                return HttpResponseRedirect(request.get_full_path())
            else:
                NewsLetterCampaign.objects.create(
                    newsletter_name=request.POST.get("newsletter_name"),
                    message=message_obj,
                )

                NewsLetterCampaign.send_email(
                    data={"users": queryset, "message_to_send": message_obj}
                )
                self.message_user(
                    request,
                    "Newsletter has been sent to {} users".format(queryset.count()),
                )
                return HttpResponseRedirect(request.get_full_path())

        messages = NewsletterMessage.objects.all()
        return render(
            request,
            "admin/send_newsletter.html",
            context={"users": queryset, "message_objects": messages},
        )

    send_newsletter.short_description = _("Wyślij newsletter")


class LandingPageAdmin(ModelAdmin):
    list_display = ("created", "promo_header", "promo_text")
    ordering = ("created",)
    exclude = ("slug",)


@admin.register(FormEntryUser)
class FormEntryAdmin(ModelAdmin):
    list_display = ("user", "get_entry_time", "get_entry_fields")

    @staticmethod
    def get_entry_time(obj):
        return obj.form_entry.entry_time

    @staticmethod
    def get_entry_fields(obj):
        _dict = {}
        for field in obj.form_entry.fields.all():
            key = Field.objects.get(id=field.field_id).label
            val = field.value
            _dict.update({key: val})
        return _dict


site.register(CustomUser, CustomUserAdmin)
site.register(LandingPage, LandingPageAdmin)


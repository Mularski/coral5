import re
from django import template

register = template.Library()


@register.filter
def add_domain_name_to_img_src(value):
    # iterator = re.finditer(r'(?<=src=")[/\w.:]+', value)
    # for match in iterator:
    #     pass
    #     # value[]
    domain = 'https://sciezkidozdrowia.pl'
    pattern = re.compile(r'(?<=src=")[/\w.:]+')
    start = -1
    while pattern.search(value, start + 1):
        match = pattern.search(value, start)
        s, e = match.regs[0]
        value = value[:s] + domain + value[s:]
        start = start + s + len(domain)


    # while value.find(letter, count + 1) > -1:
    #     count = value.find(letter, count + 1)
    #     occurences += 1
    return value


# def get_appointment(invited, user):
#     try:
#         appointment = Appointment.objects.filter(user=user, invited=invited)
#         if appointment:
#             appointment = appointment.order_by('meeting_time').first()
#             print(appointment.meeting_time.tzinfo)
#             return appointment if appointment.meeting_time > datetime.now(tz=pytz.utc) else False
#     except Appointment.DoesNotExist:
#         return False
#
#
# @register.filter
# def get_form_field(field_id):
#     try:
#         field = Field.objects.get(id=field_id)
#         if field:
#             return field
#     except Field.DoesNotExist:
#         return None

@register.filter
def order_by(query, arg):
    return query.order_by(arg or "timestamp")

@register.filter
def get_attribute(object, attr_name):
    return getattr(object, attr_name)

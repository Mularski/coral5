from django.db.models import Case, When
from django.forms.widgets import Input


class SharedListWidget(Input):
    def __init__(self, model=None, query=None, attrs=None):
        super().__init__(attrs)
        self.model = model
        self.query = query
    
    template_name = 'django/forms/widgets/shared_list.html'

    @property
    def available_options(self):
        all_objects = self.model.objects.all()
        return all_objects.exclude(id__in=self.query)

    @property
    def selected_options(self):
        all_objects = self.model.objects.all()
        query = all_objects.filter(id__in=self.query)
        preserved = Case(*[When(pk=pk, then=pos) for pos, pk in enumerate(self.query)])
        return query.order_by(preserved)

    def get_context(self, name, value, attrs):
        context = {}
        context['widget'] = {
            'name': name,
            'required': False,
            'attrs': self.build_attrs(self.attrs, attrs),
            'template_name': self.template_name,
            'type': self.input_type,
            'available_options': self.available_options,
            'selected_options': self.selected_options,
        }
        return context

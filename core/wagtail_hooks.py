from wagtail.contrib.modeladmin.options import (
    ModelAdmin, ModelAdminGroup, modeladmin_register)

from core.models import CustomUser, LandingPage
from django.utils.translation import ugettext_lazy as _


class CustomUserModelAdmin(ModelAdmin):
    model = CustomUser
    menu_label = _("Użytkownicy")  # ditch this to use verbose_name_plural from model
    menu_icon = 'user'  # change as required
    list_display = ('first_name', 'last_name', 'email', 'coral_id', 'is_subscriber', 'is_paid')
    list_filter = ('is_subscriber', 'is_paid')
    search_fields = ('first_name', 'last_name', 'coral_id', 'email')


modeladmin_register(CustomUserModelAdmin)


class LandingPageModelAdmin(ModelAdmin):
    model = LandingPage
    menu_label = _("Landing Page")  # ditch this to use verbose_name_plural from model
    menu_icon = 'document'  # change as required
    list_display = ('promo_header', 'promo_text')
    search_fields = ('promo_header', 'promo_text')


modeladmin_register(LandingPageModelAdmin)

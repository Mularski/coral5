import re
from django.core.exceptions import ValidationError
from bs4 import BeautifulSoup
from django.utils.translation import gettext_lazy as _


def check_if_less_than_200_words(value):
    soup = BeautifulSoup(value.strip(), 'html.parser')

    all_words = 0
    for line in value.strip().split('\n'):
        link_words = 0

        line_soup = BeautifulSoup(line.strip(), 'html.parser')
        for link in line_soup.findAll('a'):
            link_words += len(link.text.split())

        words_count = len(line_soup.text.split())
        all_words += (words_count + link_words) 
        
    if all_words > 200:
        raise ValidationError(
            _('Limit słów to 200. Aktualnie jest %(all_words)s.'), 
            params={'all_words': all_words}
        )

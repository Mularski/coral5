from django.test import SimpleTestCase, TestCase, Client
from django.urls import reverse
from forms_builder.forms.models import FormEntry, Form
from forms_builder.forms.views import form_detail

from core.models import FormEntryUser
from core.field_validators import check_if_less_than_200_words
from profiles.views import CustomFormDetail
from django.core.exceptions import ValidationError



class ValidatorsTestCase(SimpleTestCase):
    def test_check_if_less_than_200_words(self):
        result = check_if_less_than_200_words('some_simple_text')
        expected = None  # as test should neither return any value nor an error
        self.assertEqual(result, expected)

    def test_check_if_less_than_200_words__negative(self):
        test_value = "Some test value " * 200
        self.assertRaisesRegex(ValidationError, 'Limit słów to 200', check_if_less_than_200_words, test_value)

class AppointmentCreateTestCase(TestCase):

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.client = Client()
        cls.form = Form.objects.create(title="Tytuł")
        cls.form.save()
        FormEntryUser.objects.all().delete()
        FormEntry.objects.all().delete()

    def test_create_formentryuser(self):
        """Sprawdza czy dobrze tworzy się FormEntryUser - model relacji między FormEntry a CustomUser"""
        _url = reverse('profile:form_detail', kwargs={'slug': self.form.slug})
        self.client.post(_url)
        self.assertEqual(self.form, FormEntryUser.objects.get().form)

    def test_update_formentryuser(self):
        """Sprawdza czy dobrze nadpisuje się FormEntryUser - model relacji między FormEntry a CustomUser"""
        pass

    @classmethod
    def tearDownClass(cls):
        FormEntryUser.objects.all().delete()
        FormEntry.objects.all().delete()

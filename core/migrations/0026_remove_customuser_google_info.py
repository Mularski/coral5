# Generated by Django 2.0.3 on 2019-02-13 19:26

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0025_customuser_google_info'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='customuser',
            name='google_info',
        ),
    ]

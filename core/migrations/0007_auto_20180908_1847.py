# Generated by Django 2.0.3 on 2018-09-08 16:47

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0006_auto_20180908_1431'),
    ]

    operations = [
        migrations.AlterField(
            model_name='customuser',
            name='coral_id',
            field=models.IntegerField(blank=True, null=True),
        ),
    ]

from django import forms
from .widgets import SharedListWidget

class SharedListFormField(forms.CharField):
    def __init__(self, *, model=None, query=None, label=None, widget=SharedListWidget, **kwargs):
        super().__init__(**kwargs)
        self.model = model
        self.label = label
        self.query = query
        self.widget = widget(query=self.query, model=self.model)
        self.validators = ()

    def clean(self, value):
        value = super().clean(value)
        if value:
            splitted = value.split(',')
            return [int(option_id) for option_id in splitted]
        else:
            return []

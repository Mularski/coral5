import re
from django.core.mail import send_mail
from django.shortcuts import render
from django.http import JsonResponse

from core.models import LandingPage
from articles.models import ArticleCategory
from kitchen.models import RecipeCategory


def lading_page_detail_view(request, slug):
    if request.method == "POST":
        if re.match(r"[^@]+@[^@]+\.[^@]+", request.POST.get("email")):
            landing_page = LandingPage.objects.get(
                id=request.POST.get("landingpage_id")
            )
            first_name = request.POST.get("name")
            email = request.POST.get("email")
            send_mail(
                "Nowe wejście na landing page",
                f"Nowe wejście na Landing Page {landing_page.promo_header}, pod adresem {landing_page.get_absolute_url()}. Dane użytkownika {first_name}, {email}",
                "kontakt@sciezkidozdrowia.pl",
                ["sciezkizdrowia@gmail.com"],
            )
    return render(
        request,
        "core/landingpage_detail.html",
        context={"landingpage": LandingPage.objects.get(slug=slug)},
    )


def navbar(request):

    article_categories = {
        article.name: article.get_absolute_url()
        for article in ArticleCategory.objects.all()
    }
    recipe_categories = {
        recipe.name: recipe.get_absolute_url()
        for recipe in RecipeCategory.objects.all()
    }

    context = {
        "article_categories": article_categories,
        "recipe_categories": recipe_categories,
    }

    return JsonResponse(context)

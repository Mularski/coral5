def get_or_none(model, *args, **kwargs):
    """return model's object or None"""
    try:
        return model.objects.get(*args, **kwargs)
    except model.DoesNotExist:
        return None


def get_ip_address(request):
    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')

    if x_forwarded_for:
        ipaddress = x_forwarded_for.split(',')[-1].strip()
    else:
        ipaddress = request.META.get('REMOTE_ADDR')
    return ipaddress

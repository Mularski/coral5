from ckeditor_uploader.fields import RichTextUploadingField
from django.conf import settings
from django.db import models
from django.db.models import Q
from tinymce.models import HTMLField

from core.models import TimestampedModel


class ThreadManager(models.Manager):
    def by_user(self, user):
        qlookup = Q(first=user) | Q(second=user)
        qlookup2 = Q(first=user) & Q(second=user)
        qs = self.get_queryset().filter(qlookup).exclude(qlookup2).distinct()
        return qs

    def get_or_new(self, user, receiver_email):  # get_or_create
        sender_email = user.email
        if sender_email == receiver_email:
            return None
        qlookup1 = Q(first__email=sender_email) & Q(second__email=receiver_email)
        qlookup2 = Q(first__email=receiver_email) & Q(second__email=sender_email)
        qs = self.get_queryset().filter(qlookup1 | qlookup2).distinct()
        if qs.count() == 1:
            return qs.first(), False
        elif qs.count() > 1:
            return qs.order_by('timestamp').first(), False
        else:
            Klass = user.__class__
            user2 = Klass.objects.get(email=receiver_email)
            if user != user2:
                obj = self.model(
                    first=user,
                    second=user2
                )
                obj.save()
                return obj, True
            return None, False


class Thread(TimestampedModel):
    # TODO: zamienię na "receiver" i "sender"
    first = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, related_name='chat_thread_first')
    second = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, related_name='chat_thread_second')
    # updated = models.DateTimeField(auto_now=True)         - modified
    # timestamp = models.DateTimeField(auto_now_add=True)   - created

    objects = ThreadManager()

    # @property
    # def room_group_name(self):
    #     return f'chat_{self.id}'

    # def broadcast(self, msg=None):
    #     if msg is not None:
    #         broadcast_msg_to_chat(msg, group_name=self.room_group_name, user='admin')
    #         return True
    #     return False


class ChatMessage(models.Model):
    thread = models.ForeignKey(Thread, null=True, blank=True, on_delete=models.SET_NULL)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, verbose_name='sender', on_delete=models.CASCADE)
    message = RichTextUploadingField(config_name='frontend')
    timestamp = models.DateTimeField(auto_now_add=True)

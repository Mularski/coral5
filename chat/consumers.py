import asyncio
import json
from django.contrib.auth import get_user_model
from channels.consumer import AsyncConsumer
from channels.db import database_sync_to_async

from .models import Thread, ChatMessage


class ChatConsumer(AsyncConsumer):
    async def websocket_connect(self, event):
        # await - execute and wait for finish. While waiting for finish async loop starts another coroutine and goes
        # back to executing function with "await" whenever possible

        receiver_email = self.scope['url_route']['kwargs']['email']
        sender = self.scope['user']
        self.thread_obj = await self.get_thread(sender, receiver_email)
        self.chat_room = f"thread_{self.thread_obj.id}"
        await self.channel_layer.group_add(  # create chat for 2 people. Work if settings.CHANNELS_LAYERS
            self.chat_room,
            self.channel_name  # name generated automatically when there is settings.CHANNELS_LAYERS
        )
        await self.send({
            "type": "websocket.accept"
        })
        for message_obj in await self.get_messages(self.thread_obj):
            await self.send({
                "type": "websocket.send",
                "text": json.dumps({'message': message_obj.message, 'user': sender.id})
            })

    async def websocket_receive(self, event):
        front_text = event.get('text', None)
        if front_text:
            dict_data = json.loads(front_text)
            msg = dict_data.get('message')
            sender = self.scope['user']
            response = {
                'message': msg,
                'user': sender.id
            }
            if not msg == '':
                await self.create_chat_message(msg)
            # trigers chat_message method
            await self.channel_layer.group_send(self.chat_room, {
                "type": "chat_message",
                "text": json.dumps(response),
            })

    async def chat_message(self, event):
        # sends message in chat room
        await self.send({
            "type": "websocket.send",
            "text": event['text']
        })

    async def websocket_disconnect(self, event):
        pass

    @database_sync_to_async
    def get_thread(self, sender, receiver_email):
        return Thread.objects.get_or_new(sender, receiver_email)[0]

    @database_sync_to_async
    def create_chat_message(self, message):
        return ChatMessage.objects.create(thread=self.thread_obj, user=self.scope['user'], message=message)

    @database_sync_to_async
    def get_messages(self, thread):
        return ChatMessage.objects.filter(thread=thread)

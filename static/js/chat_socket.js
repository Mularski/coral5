var email_cache = '';

function open_chat(email, first_name='', last_name='') {

    var loc = window.location;
    var formBox = $('#chat-form');
    var msgInput = $('#id_message');
    var chatHolder = $('#chat-messages');
    var chatBox = document.getElementById('chat-div');

    chatBox.style.display = 'block';
    document.getElementById('chat-form').style.display = 'block';
    chatBox.scrollIntoView();
    chatBox.prepend(first_name + ' ', last_name + ' ', email);

    if (email_cache === email){
        console.log("Prevented opening new socket");
        email_cache = '';
        chatBox.style.display = 'none';
        document.getElementById('chat-form').style.display = 'none';
        return
    }
    else {
        email_cache = email;
        while (chatHolder[0].lastChild){
            chatHolder[0].removeChild(chatHolder[0].lastChild)
        }
    }

    var wsProtocol = (loc.protocol === 'https:') ? 'wss://' : 'ws://';
    var endpoint = wsProtocol + loc.host + loc.pathname + 'chat/' + email + '/';
    console.log(endpoint);
    var socket = new ReconnectingWebSocket(endpoint);


    socket.onmessage = function (e) {
        console.log("message", e);
        var chatDataMsg = JSON.parse(e.data);
        var user_id = document.getElementById('user-id');
        if (user_id === chatDataMsg.user && chatDataMsg.message !== '') {
            chatHolder.append(
                '<div style="background-color: #58a314; padding: 10px 10px 10px 10px; margin: 10px 10px 10px 10px"' +
                'class="col-11 offset-1 col-md-7 offset-md-3">' +
                chatDataMsg.message +
                '</div>'
            );
            chatBox.scrollTop = chatBox.scrollHeight;
        }
        else if (chatDataMsg.message !== '') {
            chatHolder.append(
                '<div style="background-color: #58a314; padding: 10px 10px 10px 10px; margin: 10px 10px 10px 10px"' +
                'class="col-11 col-md-7">' +
                chatDataMsg.message +
                '</div>'
            );
            chatBox.scrollTop = chatBox.scrollHeight;
        }
    };


    socket.onopen = function (e) {
        console.log("open", e);

        while (chatHolder.lastChild) {
            chatHolder.removeChild(chatHolder.lastChild);
        }
        formBox.submit(function (event) {
            event.preventDefault();  //if WS is disconnected send message via form/view, otherwise do it through socket
            var msgText = msgInput.val();
            socket.send(JSON.stringify({'message': msgText}));
            CKEDITOR.instances['id_message'].setData('')  //empty the message textarea
        })
    };



    socket.onerror = function (e) {
        console.log("error", e)
    };
    socket.onclose = function (e) {
        console.log("close", e)
    };

}

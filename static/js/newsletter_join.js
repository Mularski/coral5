$( document ).ready(function() {
    $("#send_newsletter_welcome").click(function () {
          var send_newsletter_welcome_url = $(this).data('href');

          $.ajax({
            url: send_newsletter_welcome_url,
            type:"POST",
            dataType: 'json',
            success: function (data) {
                $("#send_newsletter_welcome").hide();
            }
          });
        });
});
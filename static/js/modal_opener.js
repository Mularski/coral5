function open_modal(e) {
    if (e.target !== e.currentTarget) {
        var src = e.target.src;
        console.log("Hello " + src);
        if (src) {
           $('.enlargeImageModalSource').attr('src', src);
           $('#enlargeImageModal').modal('show');
        }
    }
    e.stopPropagation();
}

var theParent = document.getElementById("chat-messages");
theParent.addEventListener("click", open_modal, false);


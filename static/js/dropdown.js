window.addEventListener("DOMContentLoaded", function () {
  const url = document.getElementById("dropdown-skrypt").getAttribute("data-url");
  $.get(url, function (data) {
    let articles = $("#articles_category");
    let kitchen = $("#recipe_category");
    const article_categories = data["article_categories"];

    $(articles).append('<div class="dropdown-content"></div>');
    $(kitchen).append('<div class="dropdown-content"></div>');

    let article_dropdown = articles.find(".dropdown-content");
    for (const [key, value] of Object.entries(article_categories)) {
      article_dropdown.append("<a href=" + value + " >" + key + "</a>");
    }
    let kitchen_dropdown = kitchen.find(".dropdown-content");
    const kitchen_categories = data["recipe_categories"];
    for (const [key, value] of Object.entries(kitchen_categories)) {
      kitchen_dropdown.append("<a href=" + value + " >" + key + "</a>");
    }
  });
});
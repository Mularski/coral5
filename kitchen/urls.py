from django.urls import path
from .views import RecipeDetailView, category_list

app_name = "kitchen"

urlpatterns = [
    path('<category>/<slug>/', RecipeDetailView.as_view(), name='detail'),
    path('<category>/', category_list, name='category_list'),
]

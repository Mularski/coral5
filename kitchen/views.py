from django.shortcuts import render
from django.views.generic import DetailView

from .models import Recipe, RecipeCategory


def category_list(request, category="Bez kategorii"):
    """show all recipes by the category"""
    category_obj = RecipeCategory.objects.get(slug=category)
    object_list = Recipe.objects.filter(category=category_obj)
    return render(request, 'tile_list.html', {'object_list': object_list, 'dict_sort': "id", "show": "contents"})


class RecipeDetailView(DetailView):
    queryset = Recipe.objects.all()

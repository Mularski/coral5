from django.utils.translation import ugettext_lazy as _
from wagtail.contrib.modeladmin.options import (
    ModelAdmin, ModelAdminGroup, modeladmin_register)

from kitchen.models import Recipe, RecipeCategory


class RecipeModelAdmin(ModelAdmin):
    model = Recipe
    menu_label = _("Przepisy")
    menu_icon = 'placeholder'
    list_display = ('name', 'category', 'timestamp')
    list_filter = ('category', )
    search_fields = ('name', 'category')


class RecipeCategoryModelAdmin(ModelAdmin):
    model = RecipeCategory
    menu_label = _("Kategorie przepisów")
    menu_icon = 'doc-empty'
    list_display = ('name', )


class RecipeAdminGroup(ModelAdminGroup):
    menu_label = _("Przepisy")
    menu_icon = 'doc-empty-inverse'
    menu_order = 200
    items = (RecipeModelAdmin, RecipeCategoryModelAdmin)


modeladmin_register(RecipeAdminGroup)

from django.db import models
from django.urls import reverse
from django.db.models.signals import pre_save
from ckeditor_uploader.fields import RichTextUploadingField
from articles.utils import unique_slug_generator


class RecipeCategory(models.Model):
    name = models.CharField(max_length=255, default='', )
    slug = models.SlugField(max_length=255, blank=True, null=True)
    img = models.ImageField(upload_to='kitchen/',
                            default='kitchen/placeholder.jpg',
                            blank=True, null=True)
    description = models.TextField(default='')

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse('kitchen:category_list', kwargs={"category": self.slug})

    class Meta:
        ordering = ['-name']
        verbose_name = "kategoria przepisu"
        verbose_name_plural = "kategorie przepisów"


class Recipe(models.Model):
    name = models.CharField(max_length=120)
    slug = models.SlugField(max_length=255, blank=True, null=True)
    category = models.ForeignKey(RecipeCategory, verbose_name="Kategoria", on_delete=models.CASCADE)
    contents = models.TextField(help_text='oddziel kazdy skladnik przecinkiem')
    timestamp = models.DateTimeField(auto_now_add=True)
    description = RichTextUploadingField()
    img = models.ImageField(upload_to='kitchen/', default='kitchen/No_image_available.svg', blank=True, null=True)

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse('kitchen:detail', kwargs={"slug": self.slug, "category": self.category.slug})

    class Meta:
        ordering = ['-timestamp', '-name']
        verbose_name = 'przepis'
        verbose_name_plural = 'przepisy'

    def get_contents(self):
        return self.contents.split(',')


def rl_pre_save_receiver(sender, instance, *args, **kwargs):
    if not instance.slug:
        instance.slug = unique_slug_generator(instance)


pre_save.connect(rl_pre_save_receiver, sender=Recipe)
pre_save.connect(rl_pre_save_receiver, sender=RecipeCategory)




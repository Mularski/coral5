from coral5.admin import site
from django.contrib import admin
from .models import Recipe, RecipeCategory


class ModelAdminWithoutSlug(admin.ModelAdmin):
    exclude = ("slug",)


site.register(Recipe, ModelAdminWithoutSlug)
site.register(RecipeCategory, ModelAdminWithoutSlug)
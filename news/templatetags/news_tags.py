from django import template

from news.models import Reservation

register = template.Library()


@register.filter
def get_reservation(ticket, user):
    if user.is_authenticated:
        try:
            return Reservation.objects.get(bought_by=user, ticket=ticket)
        except Reservation.DoesNotExist:
            return False

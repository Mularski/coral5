from django import forms
from django.core.exceptions import ValidationError
from django.utils.translation import ugettext as _

from news.models import Event2, Ticket, Reservation
from core.models import CustomUser
from profiles.models import BankAccount


class CustomDateInput(forms.DateInput):
    input_type = 'date'


class CustomTimeInput(forms.TimeInput):
    input_type = 'time'


class EventForm(forms.ModelForm):
    """Used in user profile to create new event"""

    def __init__(self, user=None, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['bank_account'].choices = [('', '--')]
        # TODO: KONIECZNY REFACOTRING
        if user and isinstance(user, CustomUser):
            if BankAccount.objects.filter(user=user):
                self.fields['bank_account'].choices.extend([(account.id, account.bank_name) for account in
                                                        BankAccount.objects.filter(user=user)])
        else:
            self.fields['bank_account'].choices = [(account.id, account.bank_name) for account in
                                                        BankAccount.objects.all()]

    class Meta:
        model = Event2
        # 'description musi być na końcu ze względu na forloop w event2_form.html
        fields = ['img', 'country', 'city', 'street', 'speaker', 'name', 'event_date', 'event_time', 'bank_account', 'description']
        widgets = {
            'event_time': CustomTimeInput(),
            'event_date': CustomDateInput()
        }


class TicketForm(forms.ModelForm):
    class Meta:
        model = Ticket
        fields = ['event', 'price', 'currency', 'quantity', 'max_reservation']


class ReservationForm(forms.ModelForm):
    class Meta:
        model = Reservation
        fields = ['ticket', 'people_no', 'first_name', 'last_name', 'tel_no', 'email']

    def clean(self):
        data = super().clean()
        if data.get('people_no', None) > data.get('ticket', None).max_reservation:
            raise ValidationError(_("Masymalna ilość osób na rezerwacji nie może przekroczyć {0}").
                                  format(data.get('ticket', None).max_reservation),
                                  code='invalid_people_no',)
        return self.cleaned_data

    def clean_email(self):
        email = self.cleaned_data['email']
        ticket = self.cleaned_data['ticket']
        res = Reservation.objects.filter(email=self.cleaned_data.get('email', None), ticket=ticket)
        if res:
            raise ValidationError(_("Rezerwacja na ten adres email już istnieje"), code='invalid')
        return email


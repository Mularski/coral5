from django.urls import path
from .views import NewsDetailView, NewsListView, add_event_to_calendar

app_name = "news"

urlpatterns = [
    path('<slug>/', NewsDetailView.as_view(), name="detail"),
    path('', NewsListView.as_view(), name='list'),
    path('dodaj-do-kalendarza/<pk>', add_event_to_calendar, name='add_to_calendar'),
]

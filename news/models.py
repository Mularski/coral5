from ckeditor_uploader.fields import RichTextUploadingField
from django.conf import settings
from django.db import models
from django.db.models.signals import pre_save
from django.urls import reverse
from django.utils.translation import ugettext_lazy as _

from articles.utils import unique_slug_generator
from core.models import TimestampedModel, LocationModel, CustomUser
from profiles.models import BankAccount

from core.utils import get_or_none


def rl_pre_save_receiver(sender, instance, *args, **kwargs):
    if not instance.slug:
        instance.slug = unique_slug_generator(instance)


class NewsCategory(models.Model):
    name = models.CharField(max_length=255, default=_('Bez kategorii'))
    description = models.TextField(default='', blank=True, null=True)
    slug = models.SlugField(max_length=255, blank=True, null=True)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ['-name']
        verbose_name = _("kategoria news'a")
        verbose_name_plural = _("kategorie news'ów")


pre_save.connect(rl_pre_save_receiver, sender=NewsCategory)


class News(models.Model):
    img = models.ImageField(_("Obraz promocyjny"), default='avatar/No-image-available.jpg', upload_to='products/',
                            blank=True, null=True)
    name = models.CharField(_("Nazwa eventu"), max_length=200)
    timestamp = models.DateTimeField(auto_now_add=True)
    description = RichTextUploadingField(_("Opis"))
    category = models.ForeignKey(NewsCategory, verbose_name=_("Kategoria"), on_delete=models.CASCADE)
    slug = models.CharField(max_length=200, blank=True, null=True)

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse('news:detail', kwargs={"slug": self.slug})

    class Meta:
        ordering = ["-timestamp", "-name"]
        verbose_name = "news"
        verbose_name_plural = "newsy"


pre_save.connect(rl_pre_save_receiver, sender=News)


class Event2(TimestampedModel, LocationModel):
    img = models.ImageField(_("Zdjęcie"), default='avatar/No-image-available.jpg', upload_to='products/', blank=True,
                            null=True)
    speaker = models.CharField(_('Prowadzący'), max_length=255, blank=True, null=True)
    name = models.CharField(_('Nazwa eventu'), default=_("Podaj nazwę eventu"), max_length=255, blank=False, null=False)
    slug = models.CharField(max_length=255, blank=False, null=False)
    event_date = models.DateField(_('Data eventu'), blank=False, null=False)
    event_time = models.TimeField(_('Godzina rozpoczęcia eventu'), blank=False, null=False)
    description = RichTextUploadingField(_('Opis eventu'))
    created_by = models.ForeignKey(settings.AUTH_USER_MODEL, verbose_name=_("Użytkownik"), related_name='events',
                                   blank=True, null=True, on_delete=models.SET_NULL)
    bank_account = models.ForeignKey(BankAccount, verbose_name=_("Konto bankowe"), blank=True, null=True, on_delete=models.DO_NOTHING)

    class Meta:
        verbose_name = "event"
        verbose_name_plural = "eventy"

    def get_absolute_url(self):
        return reverse('events:detail', kwargs={'slug': self.slug})

    def __str__(self):
        return self.name

    def is_added_to_calendar(self, user):
        ev = get_or_none(EventUser, **{'event': self, 'user': user})
        return bool(ev)


pre_save.connect(rl_pre_save_receiver, sender=Event2)


class Ticket(TimestampedModel):
    event = models.ForeignKey(Event2, verbose_name=_("Wydarzenie"), on_delete=models.CASCADE, blank=True, null=True,
                              related_name='tickets')
    price = models.IntegerField(_("Cena biletu"))
    currency = models.CharField(_("Waluta biletu"), default='PLN', max_length=30)
    quantity = models.IntegerField(_("Ilość biletów"))
    max_reservation = models.IntegerField(_("Maksymalna ilość biletów na zamówienie"), default=5)

    def __str__(self):
        return self.event.name


class Reservation(TimestampedModel):
    ticket = models.ForeignKey(Ticket, verbose_name=_("Rezerwacja"), on_delete=models.CASCADE, blank=True, null=True,
                               related_name='reservations')
    people_no = models.IntegerField(_("Ilość osób dla rezerwacji"))
    bought_by = models.ForeignKey(settings.AUTH_USER_MODEL, verbose_name=_("Użytkownik"), on_delete=models.CASCADE,
                                  blank=True, null=True)
    email = models.EmailField(_("Adres email"), blank=True, null=True)
    tel_no = models.CharField(_("Numer telefonu"), blank=True, null=True, max_length=15)
    ip = models.GenericIPAddressField(blank=True, null=True)
    first_name = models.CharField(_("Imię"), blank=True, null=True, max_length=100)
    last_name = models.CharField(_("Nazwisko"), blank=True, null=True, max_length=100)

    def __str__(self):
        return 'Rezerwacja na ' + self.ticket.event.name

    def get_absolute_url(self):
        return reverse('events:detail', kwargs={'slug': self.ticket.event.slug})


class EventUser(TimestampedModel):
    event = models.ForeignKey(Event2, blank=False, null=False, on_delete=models.CASCADE)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, blank=False, null=False, on_delete=models.CASCADE)

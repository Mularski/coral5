import datetime
import json
from tempfile import TemporaryFile, NamedTemporaryFile

from django.core.exceptions import ValidationError
from django.utils.translation import ugettext as _
from googleapiclient.discovery import build
from httplib2 import Http
from oauth2client import file, client, tools
from oauth2client.client import OAuth2Credentials


def bank_number_validator(bank_number):
    no_whitespaces = bank_number.replace(" ", "")
    country_code = bank_number[0:1] if bank_number[0].isalpha() and bank_number[1].isalpha else ''
    if next((char for char in bank_number[2 if country_code else 0:] if char.isalpha()), None):
        raise ValidationError(_("Numer konta powinien składać się z cyfr i opcjonalnego dwuznakowego kodu kraju"))

    if len(no_whitespaces) != 26 and not country_code\
            or (len(no_whitespaces) != 28 and country_code):
        raise ValidationError(_("Numer konta bankowego powinien mieć 26 cyfr, opcjonalnie dwuznakowy kod kraju"
                                " na początku."))


# def create_google_calendar_event():
#     SCOPES = 'https://www.googleapis.com/auth/calendar'
#     temp_file = NamedTemporaryFile()
#     temp_file.write(open('token.json').read().encode())
#     store = file.Storage(temp_file)
#     creds = store.get()
#     if not creds or creds.invalid:
#         flow = client.flow_from_clientsecrets('credentials.json', SCOPES)
#         creds = tools.run_flow(flow, store)
#     service = build('calendar', 'v3', http=creds.authorize(Http()))
#
#     # Call the Calendar API
#     now = datetime.datetime.utcnow().isoformat() + 'Z'  # 'Z' indicates UTC time
#     super_event = {
#         "summary": "SUPER DESC",
#         "start": {
#             "dateTime": "2019-01-20T17:00:00+01:00"
#         },
#         "end": {
#             "dateTime": "2019-01-20T18:00:00+01:00"
#         }
#     }
#     super_event = json.dumps(super_event, indent=4, sort_keys=True, default=str)
#     print(type(super_event))
#     service.events().insert(calendarId='primary', body=json.loads(super_event)).execute()
#     print('Getting the upcoming 10 events')
#     events_result = service.events().list(calendarId='primary', timeMin=now,
#                                           maxResults=10, singleEvents=True,
#                                           orderBy='startTime').execute()
#     events = events_result.get('items', [])
#
#     if not events:
#         print('No upcoming events found.')
#     for event in events:
#         start = event['start'].get('dateTime', event['start'].get('date'))
#         print(start, event['summary'])
#
# if __name__ == '__main__':
#     create_google_calendar_event()


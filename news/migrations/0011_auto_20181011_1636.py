# Generated by Django 2.0.3 on 2018-10-11 14:36

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('news', '0010_event2_user'),
    ]

    operations = [
        migrations.RenameField(
            model_name='event2',
            old_name='user',
            new_name='created_by',
        ),
    ]

# Generated by Django 2.0.3 on 2018-10-09 10:13

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('news', '0007_auto_20181009_1207'),
    ]

    operations = [
        migrations.AlterField(
            model_name='event2',
            name='created',
            field=models.DateTimeField(auto_now_add=True, null=True, verbose_name='Utworzono'),
        ),
        migrations.AlterField(
            model_name='event2',
            name='modified',
            field=models.DateTimeField(auto_now=True, null=True, verbose_name='Zmodyfikowano'),
        ),
    ]

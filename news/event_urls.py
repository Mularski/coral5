from django.urls import path
from .views import EventDetailView, NewsListView

app_name = "events"

urlpatterns = [
    path('<slug>/', EventDetailView.as_view(), name="detail"),
    path('', NewsListView.as_view(), name='list')
]

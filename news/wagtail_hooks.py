from django.utils.translation import ugettext_lazy as _
from wagtail.contrib.modeladmin.options import (
    ModelAdmin, ModelAdminGroup, modeladmin_register)

from news.models import NewsCategory, News, Event2


class NewsModelAdmin(ModelAdmin):
    model = News
    menu_label = _("Nowości")
    menu_icon = 'placeholder'
    list_display = ('name', 'category', 'timestamp')
    list_filter = ('category', )
    search_fields = ('name', 'category')


class NewsCategoryModelAdmin(ModelAdmin):
    model = NewsCategory
    menu_label = _("Kategorie newsów")
    menu_icon = 'doc-empty'
    list_display = ('name', )


class EventModelAdmin(ModelAdmin):
    model = Event2
    menu_label = _("Wydarzenia")
    menu_icon = 'placeholder'
    list_display = ('name', 'timestamp', 'event_date', 'event_time')
    search_fields = ('name', )


class NewsAdminGroup(ModelAdminGroup):
    menu_label = _("Nowości i Wydarzenia")
    menu_icon = 'doc-empty-inverse'
    menu_order = 200
    items = (NewsModelAdmin, NewsCategoryModelAdmin, EventModelAdmin)


modeladmin_register(NewsAdminGroup)

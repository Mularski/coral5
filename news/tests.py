from django.test import TestCase
from rest_framework.exceptions import ValidationError

from .utils import bank_number_validator


class BankAccountTestCase(TestCase):

    def test_bank_number_validator_positive(self):
        test_cases = ["95 1160 2202 0000 0002 2724 2061",
                      "95116022020000000227242061",
                      "PL95 1160 2202 0000 0002 2724 2061",
                      "PL95116022020000000227242061"]
        for bank_number in test_cases:
            self.assertEqual(bank_number_validator(bank_number), None)

    # TODO: Dlaczego to nie działa?
    def test_bank_number_validator_negative(self):
        test_cases = ["95 1160 2202 0000 0002 2724 2061 1000",
                      "PL95 1160 0000 0004 2061",
                      "951160220200000002272420ac",
                      ]

        for bank_number in test_cases:
            with self.assertRaises(ValidationError) as e:
                bank_number_validator(bank_number)
            # self.assertRaises(ValidationError, bank_number_validator, bank_number)

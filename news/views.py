import datetime
import json

from django.shortcuts import redirect
from django.urls import reverse
from django.views.generic import DetailView, ListView
from googleapiclient.discovery import build
from httplib2 import Http
from oauth2client import file, client, tools

from .models import News, Event2


class NewsDetailView(DetailView):
    queryset = News.objects.all()


class NewsListView(ListView):
    queryset = News.objects.all()

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['event_list'] = Event2.objects.filter(event_date__gte=datetime.date.today()).order_by('event_date')
        return context


class EventDetailView(DetailView):
    queryset = Event2.objects.all().prefetch_related('tickets')


def add_event_to_calendar(request, pk):
    from .models import EventUser
    if request.user.is_authenticated:
        obj, created = EventUser.objects.get_or_create(event_id=pk, user=request.user)
        if created:
            SCOPES = 'https://www.googleapis.com/auth/calendar'
            store = file.Storage('token_' + str(request.user.id) + '.json')
            creds = store.get()
            event = Event2.objects.get(pk=pk)
            later = event.event_time.replace(hour=event.event_time.hour + 1)
            if not creds or creds.invalid:
                flow = client.flow_from_clientsecrets('credentials.json', SCOPES)
                creds = tools.run_flow(flow, store)
            service = build('calendar', 'v3', http=creds.authorize(Http()))

            event_to_add = {
                "summary": str(event.name),
                "description": event.description,
                "start": {
                    "dateTime": event.event_date.strftime('%Y-%m-%dT') + event.event_time.strftime('%H:%M:%S') + 'Z',
                },
                "end": {
                    "dateTime": event.event_date.strftime('%Y-%m-%dT') + later.strftime('%H:%M:%S') + 'Z',
                }
            }
            event_to_add = json.dumps(event_to_add, indent=4, sort_keys=True, default=str)
            service.events().insert(calendarId='primary', body=json.loads(event_to_add)).execute()
    return redirect(reverse('news:list'))

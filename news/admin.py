from coral5.admin import site
from news.models import NewsCategory, News, Event2
from django.contrib import admin



class EventAdmin(admin.ModelAdmin):
    list_display = ('event_date', 'name', 'speaker', 'city')
    search_fields = ('name', 'speaker', 'city')
    fields = ['img', 'country', 'city', 'street', 'speaker', 'name', 'event_date', 'event_time', 'bank_account', 'description']

    def save_model(self, request, obj, form, change):
        obj.created_by = request.user
        obj.save()

    class Meta:
        model = Event2


site.register(News)
site.register(NewsCategory)
site.register(Event2, EventAdmin)
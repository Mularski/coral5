from django.urls import path

from profiles.views import CustomUserUpdateView, EventUpdateView, EventListView, ticket_create_view, TicketUpdateView, \
    ReservationCreateView, ReservationUpdateView, event_delete_view, NoteListView, NoteUpdateView, NoteCreateView, \
    user_profile_view, user_activate, NoteDetailView, BankAccountCreateView, BankAccountListView, \
    BankAccountUpdateView, event_create_view, NoteUserUpdateView

app_name = "profile"

urlpatterns = [

    path('<int:pk>/zaktualizuj/', CustomUserUpdateView.as_view(), name='update'),
    path('aktywacja/<uidb64>/<token>/', user_activate, name='activate'),

    path('notatki/', NoteListView.as_view(), name='note-list'),
    path('notatki/stworz', NoteCreateView.as_view(), name='note-create'),
    path('notatki/edytuj/<pk>', NoteUpdateView.as_view(), name='note-edit'),
    path('notatki/edytuj_p/<pk>', NoteUserUpdateView.as_view(), name='note-user-edit'),
    path('notatki/<pk>', NoteDetailView.as_view(), name='note-detail'),

    path('lista-eventow/', EventListView.as_view(), name='event-list'),  # UWAGA hardcode w widoku aktualizacji biletu
    path('stworz-event/', event_create_view, name='event-create'),
    path('edytuj-event/<pk>', EventUpdateView.as_view(), name='event-edit'),

    path('lista-kont/', BankAccountListView.as_view(), name='bankaccount-list'),  # UWAGA hardcode w widoku aktualizacji biletu
    path('stworz-konto/', BankAccountCreateView.as_view(), name='bankaccount-create'),
    path('edytuj-konto/<pk>', BankAccountUpdateView.as_view(), name='bankaccount-edit'),

    path('stworz-bilet/<pk>', ticket_create_view, name='ticket-create'),  # PK eventu
    path('edytuj-bilet/<pk>', TicketUpdateView.as_view(), name='ticket-edit'),  # PK biletu
    path('usun-bilet/<pk>', event_delete_view, name='ticket-delete'),  # PK biletu

    path('stworz-rezerwacje/<pk>', ReservationCreateView.as_view(), name='reservation-create'),  # PK biletu
    path('edytuj-rezerwacje/<pk>', ReservationUpdateView.as_view(), name='reservation-edit'),  # PK rezerwacji

    path('', user_profile_view, name='user-profile'),

]

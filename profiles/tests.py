from django.core.exceptions import ValidationError
from django.test import TestCase

from core.models import CustomUser
from .utils import get_or_create_user


class ProfileViewTestCase(TestCase):

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.test_user1 = CustomUser.objects.create_user(coral_id=100100,
                                                        first_name='IN BLANCO',
                                                        last_name='Zagloba',
                                                        password='random-pass',
                                                        email='test_user1' + '@sciezkidozdrowia.pl'
                                                        )
        cls.test_user2 = CustomUser.objects.create_user(coral_id=100101,
                                                        first_name='Adam',
                                                        last_name='Mickiewicz',
                                                        password='random-pass',
                                                        email='test_user2' + '@sciezkidozdrowia.pl'
                                                        )
        cls.test_user3 = CustomUser.objects.create_user(coral_id=100102,
                                                        first_name='Pan',
                                                        last_name='Wolodyjowski',
                                                        password='random-pass',
                                                        email='test_user3' + '@sciezkidozdrowia.pl'
                                                        )
        cls.test_user1.save()
        cls.test_user2.save()
        cls.test_user3.save()

    # def test_get_or_create_user(self):
    #     # sprawdz czy user z takim coral id juz istnieje
    #     check1 = get_or_create_user('100100', email='1111@1111.com', password='some_pass')  # IN BLANCO
    #     check2 = get_or_create_user('100103', email='1112@1111.com', password='some_pass')
    #     self.assertEqual(check1.first_name, 'IN BLANCO')
    #     with self.assertRaises(ValidationError):
    #         get_or_create_user('100101', email='1113@1111.com', password='some_pass')
    #     self.assertTrue(isinstance(check2, CustomUser))

    def test_get_or_create_invited_by(self):
        check1 = get_or_create_user(100101)
        check2 = get_or_create_user(100105)
        self.assertTrue(isinstance(check1, CustomUser))
        self.assertTrue(isinstance(check2, CustomUser))
        self.assertEqual(check1.first_name, 'Adam')
        self.assertEqual(check2.first_name, 'IN BLANCO')

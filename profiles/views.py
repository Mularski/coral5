import copy
import mimetypes
import os
import re

from chat.forms import ComposeForm
from core.models import CustomUser, FormEntryUser
from core.utils import get_ip_address
from django.conf import settings
from django.contrib.auth import REDIRECT_FIELD_NAME, login
from django.contrib.auth.decorators import login_required
from django.core.mail import send_mail
from django.forms import Select
from django.http import Http404, HttpResponse, HttpResponseRedirect
from django.shortcuts import get_object_or_404, redirect, render
from django.template import RequestContext
from django.template.loader import render_to_string
from django.urls import reverse
from django.utils.encoding import force_text
from django.utils.http import urlquote, urlsafe_base64_decode
from django.utils.safestring import mark_safe
from django.views.generic import DetailView, ListView, UpdateView
from django.views.generic.edit import CreateView
from forms_builder.forms.forms import FormForForm
from forms_builder.forms.models import Field, Form, FormEntry
from forms_builder.forms.signals import form_invalid, form_valid
from forms_builder.forms.utils import split_choices
from forms_builder.forms.views import FormDetail
from news.forms import EventForm, ReservationForm, TicketForm
from news.models import Event2, Reservation, Ticket
from profiles.forms import (
    BankAccountForm,
    CustomUserChangeForm,
    CustomUserCreationForm,
    NoteUserForm,
)
from profiles.utils import (
    TokenGenerator,
    create_calendar_object,
    get_or_create_user,
    send_activation_message,
)

from .forms import NoteForm
from .models import Appointment, BankAccount, Note


class CustomUserUpdateView(UpdateView):
    template_name = "profiles/custom_user_update_form.html"
    form_class = CustomUserChangeForm
    model = CustomUser


def signup_view(request):
    if request.method == "POST":
        form = CustomUserCreationForm(request.POST, request.FILES)
        if form.is_valid():
            coral_id = request.POST.get("coral_id")
            email = request.POST.get("email")
            password = request.POST.get("password1")
            invited_by = request.POST.get("invited_by")

            new_user = (
                get_or_create_user(coral_id, email, password)
                if coral_id
                else CustomUser.objects.create_user(email=email, password=password)
            )

            new_user.invited_by = get_or_create_user(invited_by) if invited_by else None

            new_user.__dict__.update(
                {
                    "coral_id": coral_id if coral_id else None,
                    "first_name": request.POST.get("first_name"),
                    "last_name": request.POST.get("last_name"),
                    "tel_no": request.POST.get("tel_no"),
                    "is_subscriber": True
                    if request.POST.get("is_subscriber")
                    else False,
                    "is_active": False,
                }
            )  # Będzie nieaktywny póki nie kliknie w link aktywacyjny
            new_user.save()
            send_activation_message(request, new_user)
            return redirect(reverse("signup-success"))
        else:
            form = CustomUserCreationForm(request.POST, request.FILES)
        return render(request, "registration/signup.html", {"form": form})
    else:
        form = CustomUserCreationForm()
    return render(request, "registration/signup.html", {"form": form})


def user_profile_view(request):
    if request.method == "POST":
        obj, created = Appointment.objects.get_or_create(
            user=request.user,
            invited_id=request.POST.get("pk"),
            defaults={"meeting_time": request.POST.get("meeting_time")},
        )
        if not created:
            obj.meeting_time = request.POST.get("meeting_time")
        obj.save()

    if request.user.is_authenticated:
        cal_obj, prev_m, next_m = create_calendar_object(request)
        return render(
            request,
            "profiles/user_profile.html",
            {
                "form": ComposeForm,
                "year": cal_obj.year,
                "month": cal_obj.month,
                "calendar": mark_safe(cal_obj.formatmonth(withyear=True)),
                "prev_month": prev_m,
                "next_month": next_m,
            },
        )
    else:
        return redirect(reverse("login"))


def user_activate(request, uidb64, token):
    try:
        uid = force_text(urlsafe_base64_decode(uidb64))
        user = CustomUser.objects.get(pk=uid)
    except (TypeError, ValueError, OverflowError, CustomUser.DoesNotExist):
        user = None
    if user is not None and TokenGenerator().check_token(user, token):
        user.is_active = True
        user.save()
        login(request, user)
        return render(
            request,
            "thank_you.html",
            {"url": reverse("opinions:list"), "header": "Dziękujemy za rejestrację"},
        )
    else:
        return redirect(reverse("bad-token"))


def download_view(request, path):
    file_path = os.path.join(settings.MEDIA_ROOT, path)
    if os.path.exists(file_path):
        with open(file_path, "rb") as fh:
            response = HttpResponse(
                fh.read(), content_type=mimetypes.guess_type(file_path)
            )
            response[
                "Content-Disposition"
            ] = "attachment; filename=" + os.path.basename(file_path)
            return response
    raise Http404


def download_view_from_folder(request, path, file_name):
    """Widok do pobierania plików z uzupełnionych ankiet"""
    file_path = os.path.join(settings.MEDIA_ROOT, "forms", path, file_name)
    if os.path.exists(file_path):
        with open(file_path, "rb") as fh:
            response = HttpResponse(
                fh.read(), content_type=mimetypes.guess_type(file_path)
            )
            response[
                "Content-Disposition"
            ] = "attachment; filename=" + os.path.basename(file_path)
            return response
    raise Http404


class EventListView(ListView):
    """Lista eventów utworzonych przez użytkownika dostępna w panelu użytkownika"""

    model = Event2

    def get_queryset(self):
        return Event2.objects.filter(created_by=self.request.user).order_by(
            "event_date"
        )

    def post(self, request, *args, **kwargs):
        """Usuwanie eventu lub biletu"""
        if request.POST.get("del_type") == "event":
            self.model.objects.get(pk=request.POST.get("pk")).delete()
        else:
            Ticket.objects.get(pk=request.POST.get("pk")).delete()
        return redirect(request.META.get("HTTP_REFERER"))


class EventUpdateView(UpdateView):
    model = Event2
    form_class = EventForm

    def get(self, request, **kwargs):
        user = request.user
        if not (user.is_staff or user.can_add_event):
            return redirect("profile:user-profile")
        else:
            return super().get(self, request, **kwargs)

    def get_form(self, form_class=None):
        """Return an instance of the form to be used in this view."""
        if form_class is None:
            form_class = self.get_form_class()
        return form_class(self.request.user, **self.get_form_kwargs())


@login_required
def event_create_view(request):
    if request.method == "POST":
        form = EventForm(request.user, request.POST)
        if form.is_valid():
            cos = form.save()
            cos.created_by = request.user
            cos.save()
            return redirect("events:list")
    else:
        form = EventForm(request.user)
    return render(request, "news/event2_form.html", {"form": form})


@login_required
def event_update_view(request):
    if request.method == "POST":
        form = EventForm(request.user, request.POST)
        if form.is_valid():
            form.save()
            return redirect("events:list")
    else:
        form = EventForm(request.user)
    return render(request, "news/event2_form.html", {"form": form})


def ticket_create_view(request, pk):
    event = Event2.objects.get(pk=pk)
    form = TicketForm(request.POST)
    if request.method == "POST" and form.is_valid():
        form.save()
        return redirect(reverse("profile:event-list"))
    # elif not form.is_valid():
    #     # TU PRZEKIEROWANIE DO FORMULARZA ALE Z UZUPEŁNIONYMI DANYMI
    #     return redirect(reverse('profile:ticket-create', kwargs={'pk': pk}))
    elif request.method == "GET":
        data = {"event": event.pk}
        form = TicketForm(initial=data)
        return render(
            request, "news/ticket_form.html", context={"form": form, "event": event}
        )
    # return render(request, 'profiles/note_update.html', {'form': NoteForm(instance=Note.objects.get(pk=pk))})  # GET METHOD


class TicketUpdateView(UpdateView):
    model = Ticket
    form_class = TicketForm
    success_url = "/profil/lista-eventow"


class ReservationCreateView(CreateView):
    model = Reservation
    form_class = ReservationForm

    def get_initial(self):
        ticket = get_object_or_404(Ticket, pk=self.kwargs.get("pk"))
        return {"ticket": ticket}

    def form_valid(self, form):
        ticket = Ticket.objects.get(id=form.cleaned_data.get("ticket").id)
        res = Reservation(**form.cleaned_data)
        res.ip = get_ip_address(self.request)
        res.save()
        ticket.quantity -= form.cleaned_data.get("people_no", None)
        ticket.save()
        # ta konstrukcja ze względu na to, ze request.user jest tylko LazyObject'em i nie zapisze się poprawnie
        try:
            form.cleaned_data.update(
                {"bought_by": CustomUser.objects.get(id=self.request.user.id) or None}
            )
        except CustomUser.DoesNotExist:
            pass
        subject = "Potwierdzenie rezerwacji na event {0}".format(
            form.cleaned_data.get("ticket").event.name
        )
        message = render_to_string(
            "news/reservation_success.html", {"reservation": res}
        )
        message_admin = render_to_string(
            "news/reservation_success_admin.html", {"reservation": res}
        )
        from_email = "kontakt@sciezkidozdrowia.pl"

        send_to = form.cleaned_data["email"]
        send_mail(subject, message, from_email, [send_to])  # USER
        send_mail(
            subject, message_admin, from_email, ["sciezkizdrowia@gmail.com"]
        )  # ADMIN
        return HttpResponseRedirect(reverse("thank_you_reservation"))

    @staticmethod
    def get_field_names_anonymous_user():
        return ["email", "first_name", "last_name"]


class ReservationUpdateView(UpdateView):
    model = Reservation
    form_class = ReservationForm

    def get_success_url(self):
        reservation = self.model.objects.get(pk=self.kwargs.get("pk"))
        return reverse("events:detail", kwargs={"slug": reservation.ticket.event.slug})


def event_delete_view(request, pk):
    if request.user.is_authenticated:
        Event2.objects.get(pk=pk).delete()
    return redirect(request.META.get("HTTP_REFERER"))


def form_sent(request, slug, template="forms/form_sent.html"):
    """
    Nadoisany widok po wysłaniu formularza z forms_builder. Teraz jest prawidłowe kodowanie bez dziwacznych znakóœ
    """
    published = Form.objects.published(for_user=request.user)
    context = {"form": get_object_or_404(published, slug=slug)}
    return render(request, template, context=context)


class CustomFormDetail(FormDetail):
    def get(self, request, *args, **kwargs):
        context = self.get_context_data(**kwargs)
        login_required = context["form"].login_required
        if login_required and not request.user.is_authenticated:
            path = urlquote(request.get_full_path())
            bits = (settings.LOGIN_URL, REDIRECT_FIELD_NAME, path)
            return redirect("%s?%s=%s" % bits)
        return self.render_to_response(context)

    def post(self, request, *args, **kwargs):
        published = Form.objects.published(for_user=request.user)
        form = get_object_or_404(published, slug=kwargs["slug"])
        form_for_form = FormForForm(
            form, RequestContext(request), request.POST or None, request.FILES or None
        )
        if not form_for_form.is_valid():
            form_invalid.send(sender=request, form=form_for_form)
        else:
            # Attachments read must occur before model save,
            # or seek() will fail on large uploads.
            attachments = []
            for f in form_for_form.files.values():
                f.seek(0)
                attachments.append((f.name, f.read()))
            entry = form_for_form.save()
            form_valid.send(sender=request, form=form_for_form, entry=entry)
            # TODO: ODKOMENTOWAĆ TO POTEM
            # self.send_emails(request, form_for_form, form, entry, attachments)
            FormEntryUser.objects.create(
                form_entry=entry,
                user=request.user,
                is_public=False if request.POST.get("save_to_draft", None) else True,
            )
            if not self.request.is_ajax():
                return redirect(
                    form.redirect_url
                    or reverse("form_sent", kwargs={"slug": form.slug})
                )
        context = {"form": form, "form_for_form": form_for_form}
        return self.render_to_response(context)

    def send_emails(self, request, form_for_form, form, entry, attachments):
        subject = form.email_subject
        if not subject:
            subject = "%s - %s" % (form.title, entry.entry_time)
        fields = []
        for (k, v) in form_for_form.fields.items():
            value = form_for_form.cleaned_data[k]
            if isinstance(value, list):
                value = ", ".join([i.strip() for i in value])
            fields.append((v.label, value))
        context = {
            "fields": fields,
            "message": form.email_message,
            "request": request,
        }
        email_to = form_for_form.email_to()
        if email_to and form.send_email:
            render_to_string("email_extras/form_response.html", context=context)
            send_mail(
                subject,
                "Uzupełniono ankietę",
                getattr(settings, "EMAIL_HOST_USER"),
                [email_to],
            )
        email_copies = split_choices(form.email_copies)
        if email_copies:
            render_to_string("email_extras/form_response.html", context=context)
            send_mail(
                subject,
                "Uzupełniono ankietę",
                getattr(settings, "EMAIL_HOST_USER"),
                [email_to],
            )


class NoteDetailView(DetailView):
    queryset = Note.objects.all()


class NoteListView(ListView):
    model = Note

    def invited_users(self):
        """Zwraca listę zaproszonych użytkowników dla których istnieją notatki"""
        id_list = self.model.objects.filter(user=self.request.user).values_list(
            "invited_id", flat=True
        )
        return CustomUser.objects.filter(id__in=id_list)

    def filled_for(self):
        filled_for = self.model.objects.filter(user=self.request.user).values_list(
            "filled_for", flat=True
        )
        return filled_for

    def categories(self):
        all_notes = self.model.objects.filter(user=self.request.user)
        categories = (
            all_notes.exclude(category__isnull=True)
            .values_list("category__name", flat=True)
            .distinct()
        )
        return categories

    def get_queryset(self):
        note_user = self.request.GET.get("note_user", None)
        category_name = self.request.GET.get("category", None)
        queryset = None

        if re.match(r"[^@]+@[^@]+\.[^@]+", str(note_user)):
            queryset = self.model.objects.filter(
                user=self.request.user, invited=CustomUser.objects.get(email=note_user)
            )
        elif note_user:
            queryset = self.model.objects.filter(
                user=self.request.user, filled_for=note_user
            )
        else:
            queryset = self.model.objects.filter(user=self.request.user)

        if category_name:
            queryset = queryset.filter(category__name=category_name)
        return queryset

    def post(self, request, *args, **kwargs):
        """Usuwanie notatki"""
        self.model.objects.get(pk=request.POST.get("pk")).delete()
        return redirect(request.META.get("HTTP_REFERER"))


class NoteUpdateView(UpdateView):
    model = Note
    form_class = NoteForm

    def get_success_url(self):
        return reverse("profile:note-list")


class NoteUserUpdateView(UpdateView):
    model = Note
    form_class = NoteUserForm
    template_name = "profiles/note_user_update.html"

    def get_context_data(self, **kwargs):
        s = super(NoteUserUpdateView, self).get_context_data()
        return s

    def get_object(self, queryset=None):
        return super().get_object()

    def get_success_url(self):
        return reverse("profile:note-list")


class NoteCreateView(CreateView):
    model = Note
    form_class = NoteForm

    def get_form(self, form_class=None):
        form = NoteForm(**self.get_form_kwargs())
        qs = CustomUser.objects.filter(invited_by=self.request.user)
        form.fields["invited"].widget = Select(
            choices=[("", "----")] + [(i.pk, i.email) for i in qs]
        )
        if not qs:
            form.fields.pop("invited")
        return form

    def form_valid(self, form):
        self.object = form.save(commit=False)
        self.object.user = self.request.user
        self.object.save()
        return redirect(reverse("profile:note-list"))


class FormListView(ListView):
    model = Form
    template_name = "profiles/form_list.html"

    def get_queryset(self):
        return self.model.objects.all()

    def filled_forms(self):
        return FormEntryUser.objects.filter(user=self.request.user)

    def filled_forms_by_invited(self):
        invited_list = CustomUser.objects.filter(invited_by=self.request.user)
        filled_forms = FormEntryUser.objects.filter(
            user__in=invited_list, is_public=True
        )
        return filled_forms

    def post(self, request):
        """Usuwanie notatki"""
        FormEntryUser.objects.get(pk=request.POST.get("pk")).delete()
        return redirect(request.META.get("HTTP_REFERER"))


class InvitedFormListView(ListView):
    model = FormEntryUser
    template_name = "profiles/invited_form_list.html"

    def _get_invited_user(self):
        email = self.kwargs["email"]
        return CustomUser.objects.get(email=email)

    def get_queryset(self):
        user = self._get_invited_user()
        if not user.invited_by == self.request.user:
            return self.model.objects.none()
        return self.model.objects.filter(user=user, is_public=True)

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super().get_context_data(**kwargs)
        context["invited_user"] = self._get_invited_user()
        return context


class FormEntryDetailView(DetailView):
    model = FormEntry
    queryset = model.objects.all()
    template_name = "profiles/form_entry_detail.html"

    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        form_user = FormEntryUser.objects.get(form_entry=self.object).user
        if form_user != request.user:
            return redirect(reverse("home:list"))
        context = self.get_context_data(object=self.object)
        return self.render_to_response(context)


def form_entry_update_view(request, pk):
    form_instance = FormEntry.objects.get(pk=pk)
    form_entry_time = form_instance.entry_time
    form = FormForForm(form=form_instance.form, instance=form_instance, context=request)
    if request.method == "GET":
        return render(
            request,
            "profiles/form_entry_update.html",
            context={"form": form, "form_entry_time": form_entry_time},
        )
    else:
        answers_dict = copy.copy(request.POST)
        answers_dict.pop("csrfmiddlewaretoken")
        for field_entry in form_instance.fields.all():
            field = Field.objects.get(id=field_entry.field_id)  # po slugu można znaleźć
            answer = answers_dict[field.slug]
            field_entry.value = answer
            field_entry.save()

        return redirect(reverse("form-list"))


class BankAccountListView(ListView):
    model = BankAccount

    def get_queryset(self):
        return self.model.objects.all()


class BankAccountUpdateView(UpdateView):
    model = BankAccount
    form_class = BankAccountForm
    queryset = model.objects.all()


class BankAccountCreateView(CreateView):
    model = BankAccount
    queryset = model.objects.all()
    form_class = BankAccountForm

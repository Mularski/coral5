from django import forms
from django.contrib.auth.forms import UserCreationForm, UsernameField
from django.forms import DateTimeInput

from core.models import CustomUser
from .models import BankAccount
from .models import Note, Appointment


class CustomUserChangeForm(forms.ModelForm):
    avatar = forms.FileField()

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields["coral_id"] = forms.IntegerField(
            label="Coral ID", disabled=True if self.instance.coral_id else False
        )

    class Meta:
        model = CustomUser
        fields = ["avatar", "first_name", "last_name", "email", "coral_id", "tel_no"]
        field_classes = {"username": UsernameField}


class CustomUserCreationForm(UserCreationForm):

    coral_id = forms.IntegerField(label="Coral ID", required=False)
    invited_by = forms.IntegerField(label="Zaproszony przez (Coral ID)", required=False)

    class Meta:
        model = CustomUser
        fields = (
            "first_name",
            "last_name",
            "email",
            "coral_id",
            "tel_no",
            "password1",
            "password2",
            "is_subscriber",
        )
        labels = {
            "is_subscriber": "Zapisz do subskrypcji",
            "password1": "Hasło",
            "password2": "Potwierdź hasło",
        }
        help_texts = {"is_subscriber": None}

    def save(self, commit=False):
        """commit=False otherwise new user will be created whenever form is valid even when view will return errors"""
        user = super().save(commit=False)
        user.set_password(self.cleaned_data["password1"])
        if commit:
            user.save()
        return user


class CustomDateTimeInput(DateTimeInput):
    input_type = "datetime-local"


class NoteUserForm(forms.ModelForm):
    """Used both to create and update Note Model instances"""

    class Meta:
        model = Note
        fields = ["description"]


class NoteForm(forms.ModelForm):
    """Used both to create and update Note Model instances"""

    class Meta:
        model = Note
        fields = [
            "invited",
            "category",
            "filled_for",
            "meeting_time",
            "title",
            "description",
        ]

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields["meeting_time"].widget = CustomDateTimeInput()


class AppointmentForm(forms.ModelForm):
    class Meta:
        model = Appointment
        fields = ["meeting_time", "invited"]


class BankAccountForm(forms.ModelForm):
    class Meta:
        model = BankAccount
        fields = ["bank_name", "bank_number", "first_name", "last_name", "user"]

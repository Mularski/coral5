from datetime import datetime

import pytz
from django import template
from forms_builder.forms.models import Field

from core.models import FormEntryUser
from profiles.models import Appointment

register = template.Library()


@register.filter
def get_appointment(invited, user):
    try:
        appointment = Appointment.objects.filter(user=user, invited=invited)
        if appointment:
            appointment = appointment.order_by('meeting_time').first()
            print(appointment.meeting_time.tzinfo)
            return appointment if appointment.meeting_time > datetime.now(tz=pytz.utc) else None
    except Appointment.DoesNotExist:
        return None


@register.filter
def get_form_field(field_id):
    try:
        field = Field.objects.get(id=field_id)
        if field:
            return field
    except Field.DoesNotExist:
        return None


@register.filter
def get_form_user(form_entry):
    return FormEntryUser.objects.get(form_entry=form_entry).user


@register.filter
def has_filled_forms(user):
    forms = FormEntryUser.objects.filter(user=user, is_public=True)
    return bool(forms)


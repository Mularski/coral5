# Generated by Django 2.0.3 on 2018-10-21 15:47

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('profiles', '0012_auto_20181021_1735'),
    ]

    operations = [
        migrations.AddField(
            model_name='note',
            name='filled_for',
            field=models.CharField(blank=True, max_length=150, null=True, verbose_name='Uzupełniony dla'),
        ),
    ]

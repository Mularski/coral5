# Generated by Django 2.0.3 on 2018-11-06 11:12

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('profiles', '0015_appointment'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='appointment',
            options={'verbose_name': 'Wiadomość', 'verbose_name_plural': 'Wiadomości'},
        ),
        migrations.AddField(
            model_name='note',
            name='meeting_time',
            field=models.DateTimeField(blank=True, null=True, verbose_name='Data spotkania'),
        ),
    ]

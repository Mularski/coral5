# Generated by Django 2.0.3 on 2018-07-19 18:36

import ckeditor_uploader.fields
import datetime
from django.db import migrations, models
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('profiles', '0002_delete_question'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='message',
            name='timestamp',
        ),
        migrations.AddField(
            model_name='message',
            name='created',
            field=models.DateTimeField(default=datetime.datetime(2018, 7, 19, 18, 36, 36, 148553, tzinfo=utc)),
        ),
        migrations.AddField(
            model_name='message',
            name='modified',
            field=models.DateTimeField(default=datetime.datetime(2018, 7, 19, 18, 36, 36, 148610, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='message',
            name='message',
            field=ckeditor_uploader.fields.RichTextUploadingField(),
        ),
    ]

import calendar
import datetime
import locale
import random
from calendar import HTMLCalendar, day_abbr, month_name

from django.conf import settings
from django.contrib.auth.tokens import PasswordResetTokenGenerator
from django.contrib.sites.shortcuts import get_current_site
from django.core.exceptions import ValidationError
from django.core.mail import send_mail
from django.template.loader import render_to_string
from django.utils import six
from django.utils.encoding import force_bytes
from django.utils.http import urlsafe_base64_encode

from core.models import CustomUser
from news.models import Event2
from profiles.models import Appointment, Note


class Calendar(HTMLCalendar):
    def __init__(self, user, year=None, month=None):
        self.year = year
        self.month = month
        self.user = user
        self.cssclasses = ["mon", "tue", "wed", "thu", "fri", "sat", "sun"]
        self.cssclasses_weekday_head = self.cssclasses
        self.cssclass_noday = "noday"
        self.cssclass_month_head = "month"
        self.cssclass_month = "month"
        self.cssclass_year_head = "year"
        self.cssclass_year = "year"
        super(Calendar, self).__init__()

    # formats a day as a td
    # filter events by day
    def formatday(self, day, events, appointments, notes):
        events_per_day = events.filter(event_date__day=day)
        appointments_per_day = appointments.filter(meeting_time__day=day).order_by('meeting_time')
        notes_per_day = notes.filter(meeting_time__day=day).order_by('meeting_time')
        d = ''

        def _get_user_info_line(user, meeting_time):
            user_name = user.first_name + " " + user.last_name if user.first_name else \
                user.email
            time_format = '%H:%M'
            return meeting_time.strftime(time_format) + " " + user_name

        def _get_note_info_line(note):
            result = ''
            if note.invited:
                user = note.invited
                result += user.first_name + " " + user.last_name if user.first_name else \
                    user.email + " "
            result += note.title
            return result

        for note in notes_per_day:
            user_info = _get_note_info_line(note)
            d += "<i class=\"fas fa-sticky-note fa-xs\"></i>" + \
                 "<a href=" + note.get_absolute_url() + " class =\"invited-row simple_link\"');\" >" + \
                 user_info + "</a><br>"

        for appointment in appointments_per_day:
            invited_user = appointment.invited
            user_info = _get_user_info_line(invited_user, appointment.meeting_time)
            d += "<i class=\"fas fa-user fa-xs\"></i>" + \
                 "<a href=\"#\" class=\"invited-row simple_link\" onclick=\"DatePickerPopup('" + str(invited_user.pk)\
                 + "');\" >" + user_info + "</a><br>"

        for event in events_per_day:
            added = event.is_added_to_calendar(user=self.user)
            sign = '<i class="fas fa-check"></i>' if added else ''
            d += f'<li>' + sign + f'<a href="{event.get_absolute_url()}" class="simple_link">{event.name}</a></li>'

        if day != 0:
            style = "cursor: pointer;"
            onclick = f"DatePickerPopupCalendar({day});"
            return f"<div class='col'><span style='{style}' onclick={onclick} class='date'>{day}</span><ul> {d} </ul></div>"
        return '<div class="col"></div>'

    def formatweekday(self, day):
        """
        Return a weekday name as a table header.
        """
        return '<div class="col">%s</div>' % (day_abbr[day])

    # formats a week as a tr
    def formatweek(self, theweek, events, appointments, notes):
        week = ''
        for d, weekday in theweek:
            week += self.formatday(d, events, appointments, notes)
        return f'<div class="row equal-row"> {week} </div>'

    def formatweekheader(self):
        """
        Return a header for a week as a table row.
        """
        s = ''.join(self.formatweekday(i) for i in self.iterweekdays())
        return '<div class="row weekdays">%s</div>' % s

    def formatmonthname(self, theyear, themonth, withyear=True):
        """
        Return a month name as a table row.
        """
        if withyear:
            s = '%s %s' % (month_name[themonth], theyear)
        else:
            s = '%s' % month_name[themonth]
        return '<div class="row"><div class="%s col">%s</div></div>' % (
            'month', s)

    # formats a month as a table
    # filter events by year and month
    def formatmonth(self, withyear=True):
        events = Event2.objects.filter(event_date__year=self.year, event_date__month=self.month)
        appointments = Appointment.objects.filter(meeting_time__year=self.year, meeting_time__month=self.month,
                                                  user=self.user)
        notes = Note.objects.filter(meeting_time__year=self.year, meeting_time__month=self.month, user=self.user)

        cal = '<div class="calendar"> '
        cal += f'{self.formatmonthname(self.year, self.month, withyear=withyear)}\n'
        cal += f'{self.formatweekheader()}\n'
        for week in self.monthdays2calendar(self.year, self.month):
            cal += f'{self.formatweek(week, events, appointments, notes)}\n'
        cal += '</div>'
        return cal


def prev_month(d):
    first = d.replace(day=1)
    prev_month = first - datetime.timedelta(days=1)
    month = 'month=' + str(prev_month.year) + '-' + str(prev_month.month)
    return month


def next_month(d):
    days_in_month = calendar.monthrange(d.year, d.month)[1]
    last = d.replace(day=days_in_month)
    next_month = last + datetime.timedelta(days=1)
    month = 'month=' + str(next_month.year) + '-' + str(next_month.month)
    return month


def get_date(req_day):
    if req_day:
        year, month = (int(x) for x in req_day.split('-'))
        return datetime.date(year, month, day=1)
    return datetime.date.today()


class TokenGenerator(PasswordResetTokenGenerator):
    def _make_hash_value(self, user, timestamp):
        return (
                six.text_type(user.pk) + six.text_type(timestamp) +
                six.text_type(user.is_active)
        )


user_activation_token = TokenGenerator()


def send_activation_message(request, user):
    email_subject = 'Potwierdź swoją rejestrację w Ścieżkach Zdrowia'
    message = render_to_string('profiles/account_activation_message.html', {
        'user': user,
        'domain': get_current_site(request).domain,
        'uid': urlsafe_base64_encode(force_bytes(user.pk)).decode(),
        'token': user_activation_token.make_token(user),
    })
    from_email = 'kontakt@sciezkidozdrowia.pl'
    if hasattr(settings, 'EMAIL_HOST_USER'):
        from_email = settings.EMAIL_HOST_USER
    recipient_list = [user.email]
    send_mail(email_subject, message, from_email, recipient_list)


def get_or_create_user(coral_id, email=None, password=None):
    try:
        user = CustomUser.objects.get(coral_id=int(float(coral_id)))
        if user.first_name == 'IN BLANCO':
            return user
        elif email and password:
            raise ValidationError("Użytkownik o tym coral ID już istnieje.")
        else:
            return user
    except CustomUser.DoesNotExist:
        if email and password:
            return CustomUser.objects.create_user(email=email, password=password)
        else:
            return CustomUser.objects.create_user(email=str(random.getrandbits(128)) + '@sciezkizdrowia.com',
                                                  password='some_pass',
                                                  first_name='IN BLANCO', last_name='IN BLANCO',
                                                  coral_id=coral_id)


def create_calendar_object(request):
    locale.setlocale(locale.LC_ALL, 'pl_PL.utf8')
    d = get_date(request.GET.get('month', None))
    cal = Calendar(request.user, d.year, d.month)
    prev_m = prev_month(d)
    next_m = next_month(d)
    return cal, prev_m, next_m

from ckeditor_uploader.fields import RichTextUploadingField
from django.db import models
from django.urls import reverse
from django.utils.translation import ugettext_lazy as _

from core.models import CustomUser, TimestampedModel
from news.utils import bank_number_validator


class NoteCategory(models.Model):
    name = models.CharField(_('Tytuł'), max_length=255, default='Bez kategorii')
    description = models.TextField(_('Opis'), blank=True, null=True)
    slug = models.SlugField(max_length=255, blank=True, null=True)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ['-name']
        verbose_name = "Kategoria notatki"
        verbose_name_plural = "Kategorie notatek"


class Note(TimestampedModel):
    """Notatka przy uzytkowniku"""
    title = models.CharField(_("Tytuł"), max_length=150)
    category = models.ForeignKey(NoteCategory, verbose_name=_("Kategoria"), on_delete=models.SET_NULL, blank=True,
                                 null=True)
    description = RichTextUploadingField(_("Treść notatki"))
    user = models.ForeignKey(CustomUser, on_delete=models.PROTECT, related_name='user', verbose_name=_("Użytkownik"))
    invited = models.ForeignKey(CustomUser, on_delete=models.PROTECT, related_name='invited', blank=True, null=True,
                                verbose_name=_("Notatka związana z"))
    filled_for = models.CharField(_("Uzupełniony dla"), max_length=150, blank=True, null=True)
    meeting_time = models.DateTimeField(_("Data spotkania"), blank=True, null=True)

    def __str__(self):
        return self.title

    class Meta:
        verbose_name_plural = _("Notatki")
        verbose_name = _("Notatka")

    def get_absolute_url(self):
        return reverse('profile:note-detail', kwargs={"pk": self.pk})


class Message(TimestampedModel):
    """Wiadomosc w czacie"""
    message = RichTextUploadingField()
    sender = models.ForeignKey(CustomUser, related_name='sender', on_delete=models.CASCADE)
    receiver = models.ForeignKey(CustomUser, related_name='receiver', on_delete=models.CASCADE)
    read = models.BooleanField(default=False)

    class Meta:
        verbose_name = _('Wiadomość')
        verbose_name_plural = _('Wiadomości')

    def __str__(self):
        return str(self.sender) + '/' + str(self.receiver) + '/' + str(self.created)


class Appointment(TimestampedModel):
    user = models.ForeignKey(CustomUser, related_name='appoint_maker', on_delete=models.CASCADE)
    invited = models.ForeignKey(CustomUser, related_name='appointed_user', on_delete=models.CASCADE)
    meeting_time = models.DateTimeField(_("Czas spotkania"))

    class Meta:
        verbose_name = _('Wiadomość')
        verbose_name_plural = _('Wiadomości')

    def __str__(self):
        return str(self.meeting_time.strftime('%d.%m.%Y'))
        # TODO format


class BankAccount(TimestampedModel):
    bank_name = models.CharField(_("Nazwa banku"), max_length=100)
    bank_number = models.CharField(_("Numer konta"), max_length=100, validators=[bank_number_validator])
    user = models.ForeignKey(CustomUser, verbose_name=_("Właściciel konta"), on_delete=models.CASCADE)
    first_name = models.CharField(_("Imię beneficjenta"), max_length=100, blank=True, null=True,
                                  help_text=_("Nie jest wymagane gdy beneficjentem jest użytkownik"))
    last_name = models.CharField(_("Nazwisko beneficjenta"), max_length=100, blank=True, null=True,
                                 help_text=_("Nie jest wymagane gdy beneficjentem jest użytkownik"))

    def __str__(self):
        return self.bank_name + ' ' + self.bank_number

    def get_absolute_url(self):
        return reverse('profile:bankaccount-list')

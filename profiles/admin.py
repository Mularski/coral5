from coral5.admin import site
from django.contrib import admin

from profiles.models import NoteCategory


class EventAdmin(admin.ModelAdmin):
    list_display = ('name', 'description',)
    exclude = ('slug',)


site.register(NoteCategory, EventAdmin)

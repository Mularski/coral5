from django.contrib.auth.password_validation import NumericPasswordValidator


class AllowNumericPassword(NumericPasswordValidator):
    def validate(self, password, user=None):
        if password.isdigit():
            pass

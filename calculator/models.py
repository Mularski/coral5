from django.db import models
from django.utils.translation import ugettext_lazy as _
from multiselectfield import MultiSelectField


class Ingredient(models.Model):
    name = models.CharField(_('Nazwa'), max_length=100)
    kcal = models.PositiveIntegerField(_('Kalorie'))
    fat = models.PositiveIntegerField(_('Tłuszcz'))
    carbohydrates = models.PositiveIntegerField(_('Węglowodany'))
    protein = models.PositiveIntegerField(_('Białko'))

    def __str__(self):
        return self.name


class Dish(models.Model):
    name = models.CharField(_('Nazwa'), max_length=100)
    kcal = models.PositiveIntegerField(_('Kalorie'))
    ingredient_list = models.ForeignKey(Ingredient, on_delete=models.CASCADE)

    def __str__(self):
        return self.name

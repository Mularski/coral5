��    ]           �      �  )   �       5        M     Z  	   f     p     |     �     �     �     �     �     �  	   �     �     �     �     	     	     '	     ;	     I	     [	  	   a	     k	     q	     y	  
   �	  
   �	  
   �	  
   �	     �	     �	     �	     �	  	   �	     �	     �	     �	  
   
     
     !
     2
     8
  	   E
     O
     V
  2   [
  >   �
     �
     �
  
   �
     �
     �
     �
                     4     J     V     ^  0   e     �     �  	   �     �     �     �     �     �     �  
                  $     )     0     8  '   ?     g     m     r     v     �     �     �     �  7   �  7   �     +  �  /  H        M  <   V     �     �     �     �  
   �     �     �     �                 	   *     4     H     Y     t     �     �     �     �     �     �     �     �     �     �               -     >     X     ]     b     t     �  	   �     �     �     �     �  
   �     �     �            C   '  =   k     �     �     �     �     �     �     �               1     O     ]     a  :   h  	   �     �     �     �     �     �       
   
          &     4     M     T     Y     `     f  ,   n     �     �     �     �     �     �     �     �  7      :   8     s                 C       K   +            !   Q              F   @   .   =      N                X       \       
              ;   D           4      ]       8   *       -   >   S      /   O       B   1   	       "       U   W   :                  $       M   R      P          V           3   (   ?      5      #                E   I   A          2          %   ,           H      G           6   7   T   Y      [       <   Z                   L   J   )   0   '       9   &       1 entry deleted %(count)s entries deleted All An alternate URL to redirect to after form submission Back to form Button text Check box Check boxes Checked Choices Contains Contains all Contains any Date Date of birth Date/time Default value Delete selected Delete selected entries? Doesn't contain Doesn't contain all Doesn't contain any Doesn't equal Doesn't equal any Draft Drop down Email Entries Equals Equals any Expires on Export CSV Export XLS Export all entries Field Fields File upload Filter by Filter entries Form Form entries Form entry Form field entries Form field entry Forms From address Help text Hidden Home If checked, only logged in users can view the form If checked, the person entering the form will be sent an email Include Intro Is between Label Login required Message Multi line text Multi select No entries selected No entries to display Not checked Nothing Number One or more email addresses, separated by commas Order Placeholder Text Published Published from Radio buttons Redirect url Required Response Send copies to Send email Single line text Sites Slug Status Subject Submit The address the email will be sent from Title Type URL View Entries View all entries View entries View form on site Visible With published selected, won't be shown after this time With published selected, won't be shown until this time and Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=(n==1 ? 0 : (n%10>=2 && n%10<=4) && (n%100<12 || n%100>14) ? 1 : n!=1 && (n%10>=0 && n%10<=1) || (n%10>=5 && n%10<=9) || (n%100>=12 && n%100<=14) ? 2 : 3);
 1 wpis usunięty %(count)s wpisy usunięte %(count)s wpisów usuniętych Wszystko Alternatywny adres do przekierowania po wysłaniu formularza Wróć do formlarza Tekst przycisku Pole wyboru Pola wyboru Zaznaczony Opcje wyboru Zawiera Zawiera wszystkie Zawiera jakiekolwiek Data Data urodzenia Data/czas Wartość domyślna Usuń zaznaczone Usunąć zaznaczone wpisy? Nie zawiera Nie zawiera wszystkich Niezawiera któregokolwiek Nieówny Nierówny któremukolwiek Szkic Lista rozwijana Email Wpisy Równy Równy któremukolwiek Wygasa Eksportuj do CSV Eksportuj do XLS Eksportuj wszystkie wpisy Pole Pola Przesłanie pliku Filtruj używając Filtruj wpisy Formularz Wpisy formularza Wpis formularza Wpisy pola formularza Wpis pola formularza Formularze Z adresu Tekst pomocniczy Ukryte Strona główna Jeśli zaznaczone, tylko zalogowani użytkownicy zobaczą formularz Jeśli zaznaczone, osoba wysyłająca formularz otrzyma email Uwzględnij Wstęp Jest pomiędzy Etykieta Wymaga logowania Wiadomość Wiele linii tekstu Wielokrotny wybór Brak zaznaczonych wpisów Brak wpisów do wyświetlenia Niezaznaczony Nic Liczba Jeden lub więcej adresów email, oddzielonych przecinkami Porządek Placeholder Opublikowany Opublikowany od Jednokrotny wybór Adres do przekierowania Wymagane Odpowiedź Wyślij kopie do Wyślij email Pojedynczy wiersz tekstu Strony Slug Status Temat Wyślij Adres, z którego będzie wysłany formularz Tytuł Typ URL Zobacz wpisy Zobacz wszystkie wpisy Zobacz wpisy Zobacz formularz na stronie Widoczne Jeśli opublikowany, nie będzie widoczny po tym czasie Jeśli opublikowany, nie będzie widoczny przed tym czasem i 